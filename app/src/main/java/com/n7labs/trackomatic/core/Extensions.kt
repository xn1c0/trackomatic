package com.n7labs.trackomatic.core

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.n7labs.trackomatic.domain.model.types.MetricType
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Gets the notification manager from local context
 */
fun Context.getNotifyManager(): NotificationManager {
    return  getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
}

/**
 * A description of an Intent and target action to perform with it.
 * Instances of this class are created with getActivity, getActivities, getBroadcast, and getService
 * By giving a PendingIntent to another application, you are granting it the right to perform the
 * operation you have specified as if the other application was yourself.
 * A PendingIntent itself is simply a reference to a token maintained by the system describing the
 * original data used to retrieve it. This means that, even if its owning application's process is
 * killed, the PendingIntent itself will remain usable from other processes that have been given it.
 */
val Context.correctFlag:Int get() {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    } else {
        PendingIntent.FLAG_UPDATE_CURRENT
    }
}

/**
 * Converts a Long (the timestamp) to a String representing a Date with only time
 */
@SuppressLint("SimpleDateFormat")
fun Timestamp.toDateOnlyTime(): String {
    val milliseconds = this.seconds * 1000 + this.nanoseconds / 1000000
    val sdf = SimpleDateFormat("HH:mm:ss")
    val netDate = Date(milliseconds)
    return sdf.format(netDate)
}

/**
 * Converts a Long (the timestamp) to a String representing a Date
 */
@SuppressLint("SimpleDateFormat")
fun Timestamp.toDateFormat(): String {
    val milliseconds = this.seconds * 1000 + this.nanoseconds / 1000000
    val sdf = SimpleDateFormat("MM/dd/yyyy")
    val netDate = Date(milliseconds)
    return sdf.format(netDate).toString()
}

/**
 * Converts a Double to a String representing the distance (can be ms or kms)
 */
fun Double.toMeters(metricType: MetricType, precision: Int = 2) =
    when (metricType) {
        MetricType.METERS -> "%.${precision}f m".format(this)
        MetricType.KILOS -> "%.${precision}f km".format(this / 1000f)
    }

/**
 * Converts a Double to a String representing the avg speed (can be ms or kms)
 */
fun Double.toAVGSpeed(metricType: MetricType, precision: Int = 2): String =
    when (metricType) {
        MetricType.METERS -> "%.${precision}f m/s".format(this)
        MetricType.KILOS -> "%.${precision}f km/h".format(this * 3.6)
    }

/**
 * Converts a Double to a String representing the calories burned (can be ms or kms)
 */
fun Double.toCaloriesBurned(metricType: MetricType, precision: Int = 2): String =
    when (metricType) {
        MetricType.METERS -> "%.${precision}f cal".format(this)
        MetricType.KILOS -> "%.${precision}f kcal".format(this / 1000)
    }

/**
 * Converts a Long (time elapsed) to a String representing time
 */
fun Long.toFullFormatTime(includeMillis: Boolean): String {
    var milliseconds = this
    val hours = TimeUnit.MILLISECONDS.toHours(milliseconds)
    milliseconds -= TimeUnit.HOURS.toMillis(hours)
    val minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
    milliseconds -= TimeUnit.MINUTES.toMillis(minutes)
    val seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
    if (!includeMillis) {
        return "${if (hours < 10) "0" else ""}$hours:" +
                "${if (minutes < 10) "0" else ""}$minutes:" +
                "${if (seconds < 10) "0" else ""}$seconds"
    }
    milliseconds -= TimeUnit.SECONDS.toMillis(seconds)
    milliseconds /= 10
    return "${if (hours < 10) "0" else ""}$hours:" +
            "${if (minutes < 10) "0" else ""}$minutes:" +
            "${if (seconds < 10) "0" else ""}$seconds:" +
            "${if (milliseconds < 10) "0" else ""}$milliseconds"
}

/**
 * Clears the actions notification
 */
fun NotificationCompat.Builder.clearActionsNotification() {
    javaClass.getDeclaredField("mActions").apply {
        isAccessible = true
        set(this@clearActionsNotification, ArrayList<NotificationCompat.Action>())
    }
}

/**
 * Converts a List<GeoPoint> (stored by firestore) to a List<LatLng> (polyData)
 */
fun List<GeoPoint>.toLatLng(): List<LatLng> {
    val mutableList = mutableListOf<LatLng>()
    this.forEach {
        mutableList.add(LatLng(it.latitude, it.longitude))
    }
    return mutableList
}