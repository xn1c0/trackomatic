package com.n7labs.trackomatic.core

object Constants {
    /**
     * Collections
     */
    const val USERS_COLLECTION = "users"
    const val SETTINGS_COLLECTION = "settings"
    const val SESSION_COLLECTION = "sessions"
    const val PROFILES_COLLECTION = "profiles"

    /**
     * OneTap Google
     */
    const val SIGN_IN_REQUEST = "signInRequest"
    const val SIGN_UP_REQUEST = "signUpRequest"

    /**
     * Fields
     */
    const val USERID_FIELD = "userID"
    const val METRICS_FIELD = "metrics"
    const val AGE_FIELD = "age"
    const val GENDER_FIELD = "gender"
    const val HEIGHT_FIELD = "height"
    const val WEIGHT_FIELD = "weight"
    const val SESSION_ID_FIELD = "sessionID"
    const val CALORIES_FIELD = "caloriesBurned"
    const val TIMESTAMP_FIELD = "timestamp"
    const val DISTANCE_FIELD = "distanceInMeters"
    const val TIME_FIELD = "timeRunInMillis"
    const val SPEED_FIELD = "avgSpeedInMeters"
    const val POLYLINES_FIELD = "listPolyLine"
    const val MAP_CONFIG_FIELD = "mapConfig"
    const val URI_FIELD = "uri"
    const val DISPLAY_NAME_FIELD = "displayName"
    const val EMAIL_FIELD = "email"
    const val PHOTO_URL_FIELD = "photoUrl"
    const val CREATED_AT_FIELD = "createdAt"
}