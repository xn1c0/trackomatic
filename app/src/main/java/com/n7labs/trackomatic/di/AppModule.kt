package com.n7labs.trackomatic.di

import android.app.Application
import android.content.Context
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.Identity
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.core.Constants.SIGN_IN_REQUEST
import com.n7labs.trackomatic.core.Constants.SIGN_UP_REQUEST
import com.n7labs.trackomatic.data.repository.*
import com.n7labs.trackomatic.domain.adapters.FirestoreAdapter
import com.n7labs.trackomatic.domain.repository.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named

/**
 * DI for firebase and repositories
 */
@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun provideFirebaseAuth() = Firebase.auth

    @Provides
    fun provideFirebaseStorage() =  Firebase.storage

    @Provides
    fun provideFirebaseFirestore() = Firebase.firestore

    @Provides
    fun provideOneTapClient(
        @ApplicationContext
        context: Context
    ) = Identity.getSignInClient(context)

    @Provides
    @Named(SIGN_IN_REQUEST)
    fun provideSignInRequest(
        app: Application
    ) = BeginSignInRequest.builder()
        .setGoogleIdTokenRequestOptions(
            BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                .setSupported(true)
                .setServerClientId(app.getString(R.string.web_client_id)) // the web client id on firebase
                .setFilterByAuthorizedAccounts(true) // only show last used account
                .build())
        .setAutoSelectEnabled(true) // automatically select last credential
        .build()

    @Provides
    @Named(SIGN_UP_REQUEST)
    fun provideSignUpRequest(
        app: Application
    ) = BeginSignInRequest.builder()
        .setGoogleIdTokenRequestOptions(
            BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                .setSupported(true)
                .setServerClientId(app.getString(R.string.web_client_id))
                .setFilterByAuthorizedAccounts(false) // show all the account on device
                .build())
        .build()

    @Provides
    fun provideGoogleSignInOptions(
        app: Application
    ) = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(app.getString(R.string.web_client_id))
        .requestEmail()
        .build()

    @Provides
    fun provideGoogleSignInClient(
        app: Application,
        options: GoogleSignInOptions
    ) = GoogleSignIn.getClient(app, options)

    @Provides
    fun provideAuthRepository(
        auth: FirebaseAuth,
        oneTapClient: SignInClient,
        signInClient: GoogleSignInClient,
        @Named(SIGN_IN_REQUEST)
        signInRequest: BeginSignInRequest,
        @Named(SIGN_UP_REQUEST)
        signUpRequest: BeginSignInRequest,
        firestore: FirebaseFirestore,
        adapter: FirestoreAdapter
    ): AuthRepository = MyAuthRepository(
        auth = auth,
        oneTapClient = oneTapClient,
        signInClient = signInClient,
        signInRequest = signInRequest,
        signUpRequest = signUpRequest,
        firestore = firestore,
        adapter = adapter
    )

    @Provides
    fun provideFirestoreAdapter(): FirestoreAdapter = FirestoreAdapter()

    @Provides
    fun provideSessionRepository(
        auth: FirebaseAuth,
        firestore: FirebaseFirestore,
        adapter: FirestoreAdapter
    ): SessionRepository = MySessionRepository(
        auth = auth,
        firestore = firestore,
        adapter = adapter
    )

    @Provides
    fun provideSettingsRepository(
        auth: FirebaseAuth,
        firestore: FirebaseFirestore,
        adapter: FirestoreAdapter
    ): SettingsRepository = MySettingsRepository(
        auth = auth,
        firestore = firestore,
        adapter = adapter
    )

    @Provides
    fun provideProfileRepository(
        auth: FirebaseAuth,
        firestore: FirebaseFirestore,
        adapter: FirestoreAdapter
    ): ProfileRepository = MyProfileRepository(
        auth = auth,
        firestore = firestore,
        adapter = adapter
    )

    @Provides
    fun provideImageRepository(
        auth: FirebaseAuth,
        storage: FirebaseStorage,
    ): ImageRepository = MyImageRepository(
        auth = auth,
        storage = storage,
    )

}