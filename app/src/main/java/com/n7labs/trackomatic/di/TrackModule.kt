package com.n7labs.trackomatic.di

import android.content.Context
import com.n7labs.trackomatic.data.location.MyTrackingDataSource
import com.n7labs.trackomatic.data.location.TrackingDataSource
import com.n7labs.trackomatic.data.repository.MyTrackingRepository
import com.n7labs.trackomatic.data.services.SharedLocationManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * DI for TrackLocation
 */
@Module
@InstallIn(SingletonComponent::class)
object TrackModule {

    @Provides
    @Singleton
    fun provideSharedLocationManager(
        @ApplicationContext context: Context,
    ): SharedLocationManager = SharedLocationManager(
        context,
    )

    @Provides
    @Singleton
    fun provideLocationDataSource(
        sharedLocationManager: SharedLocationManager,
    ): TrackingDataSource = MyTrackingDataSource(sharedLocationManager)

    @Provides
    @Singleton
    fun provideLocationRepository(
        locationDataSource: TrackingDataSource
    ): MyTrackingRepository = MyTrackingRepository(locationDataSource)
}