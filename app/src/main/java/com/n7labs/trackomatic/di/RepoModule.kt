package com.n7labs.trackomatic.di

import com.n7labs.trackomatic.data.repository.MyTrackingRepository
import com.n7labs.trackomatic.domain.repository.TrackingRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * DI for TrackLocation
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun provideLocationRepository(
        myTrackingRepository: MyTrackingRepository
    ): TrackingRepository
}