package com.n7labs.trackomatic.ui.screens.auth.signin.components

import android.app.Activity
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import com.google.android.gms.auth.api.identity.BeginSignInResult
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.GoogleAuthProvider
import com.n7labs.trackomatic.domain.events.SignInUIEvent
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.ui.screens.auth.signin.SignInViewModel
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar

@Composable
fun SignInWithGoogle(
    viewModel: SignInViewModel,
    navToVerifyScreen: () -> Unit,
) {
    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            try {
                val credentials = viewModel.oneTapClient.getSignInCredentialFromIntent(result.data)
                val googleIdToken = credentials.googleIdToken
                val googleCredentials = GoogleAuthProvider.getCredential(googleIdToken, null)
                viewModel.onEvent(SignInUIEvent.CompleteGoogleSignIn(googleCredentials))
            } catch (e: ApiException) {
                Log.d("AUTH", e.toString())
            }
        }
    }

    fun launch(signInResult: BeginSignInResult) {
        val intent = IntentSenderRequest.Builder(signInResult.pendingIntent.intentSender).build()
        launcher.launch(intent)
    }

    when(val response = viewModel.oneTapSignInResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data != null)
                LaunchedEffect(response) {
                    launch(response.data)
                }
        }
        is Response.Failure -> LaunchedEffect(Unit) {
            Log.d("AUTH", response.toString())
        }
    }

    when(val response = viewModel.signInWithGoogleResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if (response.data == true) {
                Toast.makeText(
                    LocalContext.current,
                    "Sign in completed!",
                    Toast.LENGTH_SHORT
                ).show()
                LaunchedEffect(response) {
                    navToVerifyScreen()
                }
            }
        }
        is Response.Failure -> LaunchedEffect(Unit) {
            Log.d("AUTH", response.toString())
        }
    }
}