package com.n7labs.trackomatic.ui.screens.auth.signin

import android.os.Build
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.ImageLoader
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberAsyncImagePainter
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.request.ImageRequest
import coil.size.Size
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.domain.events.SignInUIEvent
import com.n7labs.trackomatic.ui.screens.auth.signin.components.IsAuthenticated
import com.n7labs.trackomatic.ui.screens.auth.signin.components.SignIn
import com.n7labs.trackomatic.ui.screens.auth.signin.components.SignInWithGoogle
import com.n7labs.trackomatic.ui.shared.buttons.FormTextButton
import com.n7labs.trackomatic.ui.shared.buttons.GoogleButton
import com.n7labs.trackomatic.ui.shared.buttons.SubmitButton
import com.n7labs.trackomatic.ui.shared.fields.PlainField
import com.n7labs.trackomatic.ui.shared.fields.SecretField
import com.n7labs.trackomatic.ui.theme.*

@Composable
@ExperimentalCoilApi
fun SignInScreen(
    viewModel: SignInViewModel,
    navToVerifyScreen: () -> Unit,
    navToSignUpScreen: () -> Unit,
    navToRestoreScreen: () -> Unit,
) {

    /**
     * Used to handle recomposition based on responses
     */
    IsAuthenticated(viewModel = viewModel, navToVerifyScreen)
    SignInWithGoogle(viewModel = viewModel, navToVerifyScreen)
    SignIn(viewModel = viewModel, navToVerifyScreen)

    /**
     * Beginning of composable UI
     */
    val imageLoader = ImageLoader.Builder(LocalContext.current)
        .components {
            if (Build.VERSION.SDK_INT >= 28) {
                add(ImageDecoderDecoder.Factory())
            } else {
                add(GifDecoder.Factory())
            }
        }
        .build()

    val localFocus = LocalFocusManager.current

    TrackOMaticTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = ColorPalette.Blue.medium
        ) {
            Box(
                contentAlignment = Alignment.TopCenter
            ) {
                Image(
                    modifier = Modifier.size(400.dp),
                    contentScale = ContentScale.Fit,
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(LocalContext.current)
                            .data(data = R.drawable.signin_illustration_light)
                            .apply(block = fun ImageRequest.Builder.() {
                                size(Size.ORIGINAL)
                            }).build(), imageLoader = imageLoader
                    ),
                    contentDescription = "signin_illustration"
                )
            }
            Box(
                contentAlignment = Alignment.BottomCenter
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "Welcome to Track-O-Matic",
                        color = Color.White,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                        style = TextStyle(
                            fontSize = 50.sp,
                            shadow = Shadow(
                                color = Color.Black,
                                offset = Offset(3.0f, 3.0f),
                                blurRadius = 5f
                            )
                        ),
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold,
                        fontFamily = Nova,
                    )
                    Card(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 10.dp),
                        backgroundColor = Color.White,
                        elevation = 0.dp,
                        shape = LoginBottomBoxShape.medium
                    ) {
                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            PlainField(
                                label = "Email",
                                painter = painterResource(id = R.drawable.ic_email),
                                onTextChanged = {
                                    viewModel.onEvent(SignInUIEvent.EmailChanged(it))
                                },
                                keyboardType = KeyboardType.Email,
                                errorMessage = viewModel.signInUIState.collectAsState().value.emailError,
                                imeAction = ImeAction.Next,
                                onNext = {
                                    viewModel.onEvent(SignInUIEvent.EmailFocusChanged)
                                    localFocus.moveFocus(FocusDirection.Down)
                                },
                                onDone = {}
                            )
                            SecretField(
                                label = "Password",
                                painter = painterResource(id = R.drawable.ic_lock),
                                onTextChanged = {
                                    viewModel.onEvent(SignInUIEvent.PasswordChanged(it))
                                },
                                keyboardType = KeyboardType.Password,
                                errorMessage = viewModel.signInUIState.collectAsState().value.passwordError,
                                imeAction = ImeAction.Done,
                                onNext = {},
                                onDone = {
                                    viewModel.onEvent(SignInUIEvent.PasswordFocusChanged)
                                    localFocus.clearFocus()
                                }
                            )
                            SubmitButton(
                                onClick = { viewModel.onEvent(SignInUIEvent.EmailSignIn) },
                                text = "Signin"
                            )
                            GoogleButton(
                                onClick = { viewModel.onEvent(SignInUIEvent.BeginGoogleSignIn) }
                            )
                            Row{
                                FormTextButton(
                                    onClick = { navToRestoreScreen() },
                                    text = "Forgot Password?",
                                    color = TextPalette.dark
                                )
                                Spacer(modifier = Modifier.width(30.dp))
                                FormTextButton(
                                    onClick = { navToSignUpScreen() },
                                    text = "Not yet registered?",
                                    color = TextPalette.dark
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}