package com.n7labs.trackomatic.ui.shared.fields

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette

@Composable
 fun <T> SelectOptionConfig(
    textField: String,
    selected: String,
    listItems: List<T>,
    listNamed: List<String>? = null,
    onChange: (T) -> Unit,
) {

    val (isDropDown, changeIsDrop) = rememberSaveable { mutableStateOf(false) }
    Row(
        modifier = Modifier.padding(horizontal = 10.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = textField,
            color = TextPalette.light,
            textAlign = TextAlign.Center,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .weight(.5f)
                .clickable { changeIsDrop(true) }
        )
        Spacer(modifier = Modifier.width(15.dp))
        Box(
            modifier = Modifier.weight(.5f)
        ) {
            OutlinedTextField(
                enabled = false,
                value = selected,
                onValueChange = {},
                readOnly = true,
                modifier = Modifier.border(
                    width = 1.dp,
                    color = ColorPalette.Blue.medium,
                    shape = RoundedCornerShape(5.dp)
                ).clickable { changeIsDrop(true) },
                singleLine = true,
                trailingIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_drop),
                        contentDescription = "DropDown"
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    disabledBorderColor = ColorPalette.Blue.medium,
                    disabledTextColor = TextPalette.light,
                    disabledTrailingIconColor = ColorPalette.Blue.medium,
                )
            )
            DropdownMenu(
                modifier = Modifier.background(ColorPalette.grey1),
                expanded = isDropDown,
                onDismissRequest = { changeIsDrop(false) }
            ) {
                listItems.forEachIndexed { index, item ->
                    DropdownMenuItem(
                        onClick = {
                            onChange(item)
                            changeIsDrop(false)
                        }
                    ) {
                        Text(
                            text = listNamed?.get(index) ?: item.toString(),
                            color = TextPalette.light
                        )
                    }
                }
            }
        }
    }
}