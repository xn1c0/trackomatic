package com.n7labs.trackomatic.ui.screens.main.home

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.DeleteSessionResponse
import com.n7labs.trackomatic.domain.repository.SessionResponse
import com.n7labs.trackomatic.domain.repository.SettingsResponse
import com.n7labs.trackomatic.domain.usecase.images.DeleteImage
import com.n7labs.trackomatic.domain.usecase.session.DeleteSession
import com.n7labs.trackomatic.domain.usecase.session.GetSessionsByDate
import com.n7labs.trackomatic.domain.usecase.setting.GetSettings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getSessionsByDate: GetSessionsByDate,
    private val deleteSession: DeleteSession,
    private val deleteImage: DeleteImage,
    private val getSettings: GetSettings
): ViewModel() {

    var sessionsResponse by mutableStateOf<SessionResponse>(Response.Loading)
        private set

    var deleteSessionResponse by mutableStateOf<DeleteSessionResponse>(Response.Success(false))
        private set

    var deleteImageResponse by mutableStateOf<DeleteSessionResponse>(Response.Success(false))
        private set

    var settingsResponse by mutableStateOf<SettingsResponse>(Response.Loading)
        private set

    init {
        gSettings()
        gSessions()
    }

    private fun gSettings() = viewModelScope.launch {
        getSettings().collect { response ->
            settingsResponse = Response.Loading
            settingsResponse = response
        }
    }

    private fun gSessions() = viewModelScope.launch {
        getSessionsByDate().collect { response ->
            sessionsResponse = Response.Loading
            sessionsResponse = response
        }
    }

    fun dSession(sessionID: String) = viewModelScope.launch {
        deleteSessionResponse = Response.Loading
        deleteSessionResponse = deleteSession(sessionID)
    }

    fun dImage(uri: String) = viewModelScope.launch {
        deleteImageResponse = Response.Loading
        deleteImageResponse = deleteImage(uri)
    }

}