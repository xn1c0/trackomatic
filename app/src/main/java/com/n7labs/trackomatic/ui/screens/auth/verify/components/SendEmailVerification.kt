package com.n7labs.trackomatic.ui.screens.auth.verify.components

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.ui.screens.auth.verify.VerifyViewModel
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar

@Composable
fun SendEmailVerification(
    viewModel: VerifyViewModel
) {
    when (val response = viewModel.sendEmailVerificationResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data == true) {
                Toast.makeText(
                    LocalContext.current,
                    "Verification email sent to your inbox",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        is Response.Failure -> {
            Toast.makeText(
                LocalContext.current,
                response.e.toString().substringAfter(": "),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}