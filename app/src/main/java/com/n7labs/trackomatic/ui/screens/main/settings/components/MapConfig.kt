package com.n7labs.trackomatic.ui.screens.main.settings.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.maps.android.compose.MapType
import com.n7labs.trackomatic.domain.model.types.PolyLineWidthType
import com.n7labs.trackomatic.ui.shared.dialogs.DialogColorPicker
import com.n7labs.trackomatic.ui.shared.fields.SelectOptionConfig
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette


@Composable
fun MapSettings(
    style: MapType,
    lineWeight: PolyLineWidthType,
    lineColor: Color,
    changeWeight: (PolyLineWidthType) -> Unit,
    changeColorMap: (Color) -> Unit,
    changeMapType: (MapType) -> Unit,
) {
    val (isDialogShow, changeVisibleDialog) = rememberSaveable {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = "Map",
            color = TextPalette.light,
            textAlign = TextAlign.Center,
            style = TextStyle(
                fontSize = 20.sp,
            )
        )
        Spacer(modifier = Modifier.height(20.dp))
        MapFromConfig(
            style = style,
            lineWeight = lineWeight,
            lineColor = lineColor,
        )
        Spacer(modifier = Modifier.height(20.dp))
        SelectMapStyle(
            currentStyle = style,
            changeStyleMap = changeMapType
        )
        Spacer(
            modifier = Modifier.height(20.dp)
        )
        SelectMapWeight(
            currentWeightMap = lineWeight,
            changeWeight = changeWeight
        )
        Spacer(
            modifier = Modifier.height(20.dp)
        )
        SelectMapColor(
            currentColor = lineColor,
            showDialogColor = { changeVisibleDialog(true) }
        )
    }

    if (isDialogShow)
        DialogColorPicker(
            hiddenDialog = { changeVisibleDialog(false) },
            changeColor = changeColorMap,
        )
}


@Composable
private fun SelectMapWeight(
    currentWeightMap: PolyLineWidthType,
    changeWeight: (PolyLineWidthType) -> Unit,
) {
    val listWeight = remember { PolyLineWidthType.values().toList() }
    SelectOptionConfig(
        textField = "Line width",
        selected = currentWeightMap.name,
        listItems = listWeight,
        listNamed = listWeight.map { it.name },
        onChange = changeWeight
    )
}

@Composable
private fun SelectMapStyle(
    currentStyle: MapType,
    changeStyleMap: (MapType) -> Unit,
) {
    var listMaps = remember { MapType.values().toList().subList(1, MapType.values().size) }
    SelectOptionConfig(
        textField = "Map type",
        selected = currentStyle.name,
        listItems = listMaps,
        listNamed = (listMaps.map { it.name }),
        onChange = changeStyleMap
    )
}

@Composable
fun SelectMapColor(
    currentColor: Color,
    modifier: Modifier = Modifier,
    showDialogColor: () -> Unit,
) {

    Row(modifier = modifier
        .padding(horizontal = 10.dp)
        .height(50.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center) {
        Text(
            text = "Line color",
            color = TextPalette.light,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .weight(.5f)
        )
        Spacer(modifier = Modifier.width(15.dp))
        Box(
            modifier = Modifier
            .weight(.5f)
            .fillMaxHeight()
            .border(
                width = 2.dp,
                color = ColorPalette.Blue.medium,
                shape = RoundedCornerShape(5.dp)
            )
            .padding(2.dp)
            .background(currentColor)
            .clickable { showDialogColor() }
        )
    }

}


