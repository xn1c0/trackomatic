package com.n7labs.trackomatic.ui.shared.graph

enum class BarType {
    CIRCULAR_TYPE,
    TOP_CURVED
}