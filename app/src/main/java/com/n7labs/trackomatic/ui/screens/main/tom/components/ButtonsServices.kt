package com.n7labs.trackomatic.ui.screens.main.tom.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.domain.model.TrackingState
import com.n7labs.trackomatic.ui.screens.main.tom.TrackingActions

@Composable
fun ButtonsServices(
    startEnabled: Boolean,
    actionServices: (TrackingActions) -> Unit,
    servicesState: TrackingState
) {
    if (servicesState == TrackingState.WAITING || servicesState == TrackingState.PAUSE) {
        Button(
            onClick = { actionServices(TrackingActions.START) },
            modifier = Modifier.defaultMinSize(minWidth = 56.dp, minHeight = 56.dp),
            enabled = startEnabled,
            shape = CircleShape
        ){
            Icon(
                painter = painterResource(id = R.drawable.ic_play),
                contentDescription = "play_icon"
            )
        }
    }

    if (servicesState == TrackingState.TRACKING) {
        Button(
            onClick = { actionServices(TrackingActions.RESUME) },
            modifier = Modifier.defaultMinSize(minWidth = 56.dp, minHeight = 56.dp),
            enabled = startEnabled,
            shape = CircleShape

        ){
            Icon(
                painter = painterResource(id = R.drawable.ic_pause),
                contentDescription = "resume_icon"
            )
        }
    }

    if (servicesState != TrackingState.WAITING) {
        Spacer(modifier = Modifier.size(20.dp))
        Button(
            onClick = { actionServices(TrackingActions.SAVED) },
            modifier = Modifier.defaultMinSize(minWidth = 56.dp, minHeight = 56.dp),
            enabled = startEnabled,
            shape = CircleShape

        ){
            Icon(
                painter = painterResource(id = R.drawable.ic_stop),
                contentDescription = "stop_icon"
            )
        }
    }
}