package com.n7labs.trackomatic.ui.shared.bars

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.n7labs.trackomatic.ui.theme.ColorPalette

@Composable
fun ProgressBar() {
    Box(
        modifier = Modifier.fillMaxSize().zIndex(100f),
        contentAlignment = Alignment.Center,
    ) {
        CircularProgressIndicator(
            modifier = Modifier.padding(all = 5.dp),
            color = ColorPalette.orange,
        )
    }
}