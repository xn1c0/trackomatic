package com.n7labs.trackomatic.ui.shared.dialogs

import android.content.Context
import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.compose.*
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.core.*
import com.n7labs.trackomatic.domain.model.Session
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.domain.model.types.PolyLineWidthType
import com.n7labs.trackomatic.ui.screens.main.settings.components.includeAll
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DialogStatistics(
    hiddenDialog: () -> Unit,
    deleteSession: (id: String, uri: String) -> Unit,
    session: Session,
    metricType: MetricType,
    modifier: Modifier = Modifier,
) {

    Log.d("SSS", session.toString())

    Dialog(
        properties = DialogProperties(
            usePlatformDefaultWidth = false,
            dismissOnBackPress = false,
            dismissOnClickOutside = false
        ),
        onDismissRequest = hiddenDialog,
    ) {
        Surface(modifier = Modifier.fillMaxSize()) {
            Column(modifier = Modifier.fillMaxSize()) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 10.dp)
                        .padding(horizontal = 10.dp)
                        .clip(MaterialTheme.shapes.small)
                        .weight(1f)
                ) {
                    GMap(
                        mapType = session.mapConfig.style,
                        polyData = session.listPolyLine!!.toLatLng(),
                        polyLineWidth = session.mapConfig.weight,
                        polyLineColor = Color(session.mapConfig.colorValue),
                        alignmentButton = Alignment.BottomEnd,
                    )
                }
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(.7f)
                ) {
                    Card(
                        shape = MaterialTheme.shapes.small,
                        backgroundColor = ColorPalette.grey1,
                        modifier = Modifier
                            .padding(all = 10.dp)
                            .fillMaxSize(),
                        elevation = 3.dp,
                    ) {
                        Column() {
                            TitleStatistics(
                                fontSizeTitle = 20.sp,
                            )
                            InfoSession(
                                session = session,
                                modifier = Modifier.padding(10.dp),
                                metricType = metricType,
                                fontSize = 15.sp
                            )
                            Row(
                                modifier = Modifier
                                    .padding(top = 20.dp)
                                    .fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Button(
                                    onClick = {
                                        deleteSession(session.sessionID!!, session.uri!!)
                                        hiddenDialog()
                                    }
                                ) {
                                    Text(text = "Delete")
                                }
                                Spacer(modifier = Modifier.width(20.dp))
                                OutlinedButton(
                                    onClick = {
                                        hiddenDialog()
                                    }
                                ) {
                                    Text(text = "Quit")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun GMap(
    mapType: MapType,
    polyData: List<LatLng>,
    polyLineWidth: PolyLineWidthType,
    polyLineColor: Color,
    alignmentButton: Alignment,
) {

    var mapLoaded by remember { mutableStateOf (false) }

    val cameraPositionState = remember {
        CameraPositionState()
    }

    var uiSettings by remember { mutableStateOf(
        MapUiSettings(
            mapToolbarEnabled = false,
            myLocationButtonEnabled = false,
            zoomControlsEnabled = false,
        )
    ) }

    var properties by remember {
        mutableStateOf(
            MapProperties(
                isMyLocationEnabled = false,
                mapType = mapType,
            )
        )
    }

    LaunchedEffect(key1 = mapLoaded) {
        cameraPositionState.animate(
            update = CameraUpdateFactory.newLatLngBounds(LatLngBounds.builder().includeAll(polyData).build(),50),
            durationMs = 1000
        )
    }

    Box(modifier = Modifier.fillMaxSize()) {
        GoogleMap(
            modifier = Modifier.fillMaxSize(),
            cameraPositionState = cameraPositionState,
            properties = properties,
            uiSettings = uiSettings,
            onMapLoaded = { mapLoaded = !mapLoaded }
        ) {
            Polyline(
                points = polyData,
                width = polyLineWidth.value,
                color = polyLineColor
            )
        }

        FloatingActionButton(
            modifier = Modifier
                .padding(10.dp)
                .size(45.dp)
                .align(alignmentButton),
            onClick = { mapLoaded = !mapLoaded }
        ) {
            Icon(
                painter = painterResource(
                    id = R.drawable.ic_location
                ),
                contentDescription = null
            )
        }
    }
}

@Composable
private fun TitleStatistics(
    fontSizeTitle: TextUnit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(5.dp),
            text = "Statistics",
            style = MaterialTheme.typography.h5.copy(fontSize = fontSizeTitle),
            color = ColorPalette.Blue.medium
        )
    }
}

@Composable
private fun InfoSession(
    session: Session,
    fontSize: TextUnit,
    metricType: MetricType,
    modifier: Modifier = Modifier,
    context: Context = LocalContext.current
) {
    val date = remember { session.timestamp.toDateFormat() }
    val timeDay = remember { session.timestamp.toDateOnlyTime() }
    val speed = remember { session.avgSpeedInMeters.toAVGSpeed(metricType) }
    val distance = remember { session.distanceInMeters.toMeters(metricType) }
    val calories = remember { session.caloriesBurned.toCaloriesBurned(metricType) }
    val timeRun = remember { session.timeRunInMillis.toFullFormatTime(true) }

    Column(modifier = modifier.verticalScroll(rememberScrollState())) {
        TitleAndInfo("Date", date, fontSize)
        TitleAndInfo("Time", timeDay, fontSize)
        TitleAndInfo("Duration", timeRun, fontSize)
        TitleAndInfo("Distance", distance, fontSize)
        TitleAndInfo("Average speed", speed, fontSize)
        TitleAndInfo("Calories", calories, fontSize)
    }

}

@Composable
private fun TitleAndInfo(
    info: String,
    title: String,
    fontSize: TextUnit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = info,
            style = MaterialTheme.typography.body1.copy(fontSize = fontSize),
            color = TextPalette.light
        )
        Spacer(modifier = Modifier.width(10.dp))
        Text(
            text = title,
            style = MaterialTheme.typography.body1.copy(fontSize = fontSize),
            color = TextPalette.light
        )
    }
}