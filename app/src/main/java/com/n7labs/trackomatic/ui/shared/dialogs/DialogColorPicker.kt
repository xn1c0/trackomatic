package com.n7labs.trackomatic.ui.shared.dialogs

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.github.skydoves.colorpicker.compose.ColorEnvelope
import com.github.skydoves.colorpicker.compose.ColorPickerController
import com.github.skydoves.colorpicker.compose.HsvColorPicker
import com.github.skydoves.colorpicker.compose.rememberColorPickerController
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette

@Composable
fun DialogColorPicker(
    hiddenDialog: () -> Unit,
    changeColor: (Color) -> Unit,
) {
    val controller = rememberColorPickerController()
    controller.setWheelColor(ColorPalette.grey1)

    AlertDialog(
        onDismissRequest = hiddenDialog,
        backgroundColor = ColorPalette.grey1,
        confirmButton = {
            Button(onClick = {
                changeColor(controller.selectedColor.value)
                hiddenDialog()
            }) {
                Text(text = "Accept")
            }
        },
        dismissButton = {
            OutlinedButton(onClick = hiddenDialog) {
                Text(text = "Cancel")
            }
        },
        title = { BodySelectColor(controller = controller) },
    )
}

@Composable
private fun BodySelectColor(
    controller: ColorPickerController,
) {
    var textColor by remember { mutableStateOf("") }

    Column {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = "Select color",
                color = TextPalette.light,
                style = MaterialTheme.typography.h6,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            )
            HsvColorPicker(
                modifier = Modifier
                    .size(250.dp)
                    .padding(10.dp),
                controller = controller,
                onColorChanged = { colorEnvelope: ColorEnvelope ->
                    textColor = colorEnvelope.hexCode
                }
            )
            InfoColorSelected(textColor = textColor,
                colorValue = controller.selectedColor.value)
        }
    }
}

@Composable
private fun InfoColorSelected(
    modifier: Modifier = Modifier,
    textColor: String,
    colorValue: Color,
) {
    Column(modifier = modifier.padding(20.dp),
        horizontalAlignment = Alignment.CenterHorizontally) {
        Text(text = "#$textColor",
            style = MaterialTheme.typography.caption,
            color = colorValue
        )
        Spacer(modifier = Modifier.height(10.dp))
        Box(
            modifier = Modifier
                .size(35.dp)
                .border(
                    width = 2.dp,
                    color = if (isSystemInDarkTheme()) Color.LightGray else Color.DarkGray,
                    shape = RoundedCornerShape(5.dp)
                )
                .padding(2.dp)
                .background(colorValue)
        )
    }
}