package com.n7labs.trackomatic.ui.screens.main.profile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.SessionResponse
import com.n7labs.trackomatic.domain.repository.SettingsResponse
import com.n7labs.trackomatic.domain.usecase.session.GetSessionsByDate
import com.n7labs.trackomatic.domain.usecase.setting.GetSettings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val getSessionsByDate: GetSessionsByDate,
    private val getSettings: GetSettings
): ViewModel() {

    var sessionsResponse by mutableStateOf<SessionResponse>(Response.Loading)
        private set

    var settingsResponse by mutableStateOf<SettingsResponse>(Response.Loading)
        private set

    init {
        gSetting()
        gSessions()
    }


    // Keeps updating data on change see profile repo
    private fun gSetting() = viewModelScope.launch {
        getSettings().collect { response ->
            settingsResponse = response
        }
    }

    private fun gSessions() = viewModelScope.launch {
        getSessionsByDate().collect { response ->
            sessionsResponse = response
        }
    }

}