package com.n7labs.trackomatic.ui.navigation.bars

import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.n7labs.trackomatic.ui.theme.ColorPalette

@Composable
fun TopBarScreen(navController: NavHostController) {
    TopMenu(navController)
}

@Composable
fun TopMenu(
    navController: NavHostController,
    modifier: Modifier = Modifier,
    activeHighlightColor: Color = ColorPalette.Blue.light,
    activeTextColor: Color = Color.White,
    inactiveTextColor: Color = Color.White,
) {
    val barContent = listOf(
        BottomBarContent.Home,
        BottomBarContent.ToM,
        BottomBarContent.Profile,
        BottomBarContent.Settings,
    )
    /**
     * Choose when to show the bottom bar based on current destination
     * Subscribe to navBackStackEntry, required to get current route!
     */
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination
    var i = -1
    barContent.forEachIndexed { index, content ->
        if(content.route == currentDestination?.route)
            i = index
    }
    if (i != -1) {
        TopAppBar(
            title = {
                Text(
                    barContent[i].title,
                    color = activeTextColor,
                )
            },
            backgroundColor = ColorPalette.Blue.medium,
        )
    }
}