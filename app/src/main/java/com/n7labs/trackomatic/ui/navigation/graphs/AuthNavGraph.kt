package com.n7labs.trackomatic.ui.navigation.graphs

import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import coil.annotation.ExperimentalCoilApi
import com.n7labs.trackomatic.ui.screens.auth.restore.RestoreScreen
import com.n7labs.trackomatic.ui.screens.auth.signin.SignInScreen
import com.n7labs.trackomatic.ui.screens.auth.signup.SignUpScreen
import com.n7labs.trackomatic.ui.screens.auth.verify.VerifyScreen

@OptIn(ExperimentalCoilApi::class)
fun NavGraphBuilder.authNavGraph(
    navController: NavHostController
) {
    navigation(
        route = Graph.AUTHENTICATION,
        startDestination = AuthScreen.SignIn.route
    ) {
        composable(route = AuthScreen.SignIn.route) {
            SignInScreen(
                viewModel = hiltViewModel(),
                navToRestoreScreen = { navController.navigate(AuthScreen.Restore.route) },
                navToSignUpScreen = { navController.navigate(AuthScreen.SignUp.route) },
                navToVerifyScreen = { navController.navigate(AuthScreen.Verify.route) },
            )
        }
        composable(route = AuthScreen.SignUp.route) {
            SignUpScreen(
                viewModel = hiltViewModel(),
                navToSignInScreen = {
                    navController.navigate(AuthScreen.SignIn.route) {
                        popUpTo(Graph.AUTHENTICATION) {
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(route = AuthScreen.Restore.route) {
            RestoreScreen(
                viewModel = hiltViewModel(),
                navToSignInScreen = {
                    navController.navigate(AuthScreen.SignIn.route) {
                        popUpTo(Graph.AUTHENTICATION) {
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(route = AuthScreen.Verify.route) {
            VerifyScreen(
                viewModel = hiltViewModel(),
                navToSignInScreen = {
                    navController.navigate(AuthScreen.SignIn.route) {
                        popUpTo(Graph.AUTHENTICATION) {
                            inclusive = true
                        }
                    }
                },
                navToHomeScreen = {
                    navController.navigate(Graph.MAIN) {
                        popUpTo(Graph.AUTHENTICATION) {
                            inclusive = true
                        }
                    }
                },
            )
        }
    }
}

sealed class AuthScreen(val route: String) {
    object SignIn : AuthScreen(route = "sign_in")
    object SignUp : AuthScreen(route = "sign_up")
    object Restore : AuthScreen(route = "restore")
    object Verify : AuthScreen(route = "verify")
}