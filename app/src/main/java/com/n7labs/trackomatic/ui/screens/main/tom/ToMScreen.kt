package com.n7labs.trackomatic.ui.screens.main.tom

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.compose.*
import com.n7labs.trackomatic.core.*
import com.n7labs.trackomatic.data.location.TrackingServices
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.ui.screens.main.settings.components.includeAll
import com.n7labs.trackomatic.ui.screens.main.tom.components.ButtonsServices
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar
import com.n7labs.trackomatic.ui.shared.dialogs.SaveDialog
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette
import com.smarttoolfactory.screenshot.ImageResult
import com.smarttoolfactory.screenshot.ScreenshotBox
import com.smarttoolfactory.screenshot.rememberScreenshotState
import kotlinx.coroutines.delay

@RequiresApi(Build.VERSION_CODES.O)
@Composable
@ExperimentalCoilApi
fun ToMScreen(
    viewModel: ToMViewModel,
    navToHomeScreen:() -> Unit
) {

    /**
     * Used to handle recomposition based on responses
     */

    val applicationContext = LocalContext.current.applicationContext
    var sessionSaved by remember { mutableStateOf (false) }

    LaunchedEffect(key1 = sessionSaved) {
        if (sessionSaved) {
            navToHomeScreen()
        }
    }

    val (isDialogShow, changeVisibleDialog) = rememberSaveable {
        mutableStateOf(false)
    }

    var settings by remember { mutableStateOf(Settings()) }

    when(val response = viewModel.settingsResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {
            if (response.data != null) {
                settings = settings.copy(
                    mapConfig = response.data.mapConfig,
                    metrics = response.data.metrics,
                    userID = response.data.userID
                )
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    when (val response = viewModel.insertImageResponse) {
        is Response.Loading -> {}
        is Response.Success -> {
            if (response.data != null) {
                viewModel.submitSession(settings, response.data)
                TrackingServices.finishServices(applicationContext)
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    when (val response = viewModel.insertSessionResponse) {
        is Response.Loading -> {}
        is Response.Success -> {
            if (response.data != false) {
                sessionSaved = true
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    val servicesState by viewModel.stateTracking.collectAsState()
    val lastLocation by viewModel.lastLocation.collectAsState()
    val polyData by viewModel.polyData.collectAsState()
    val timeRun by viewModel.timeRun.collectAsState()

    var bounds by remember {
        mutableStateOf<LatLngBounds?>(null)
    }

    val cameraPositionState = rememberCameraPositionState {}
    lastLocation?.let {
        LaunchedEffect(key1 = lastLocation) {
            cameraPositionState.animate(CameraUpdateFactory.newLatLngZoom(lastLocation!!, 18f))
        }
    }

    val screenshotState = rememberScreenshotState()
    val imageResult: ImageResult = screenshotState.imageState.value

    LaunchedEffect(key1 = bounds) {
        if(bounds != null) {
            cameraPositionState.move(
                update = CameraUpdateFactory.newLatLngBounds(
                    bounds!!,
                    500,
                    500,
                    50
                )
            )
            changeVisibleDialog(true)
            delay(4000) // must give time to update map
            screenshotState.capture()
            bounds = null
        }
    }

    Column(
        modifier = Modifier.fillMaxSize(1f),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Card(
            shape = MaterialTheme.shapes.small,
            backgroundColor = ColorPalette.grey1,
            modifier = Modifier
                .padding(top = 10.dp)
                .padding(horizontal = 10.dp)
                .fillMaxWidth()
                .weight(.7f),
            elevation = 3.dp,
        ) {
            ScreenshotBox(screenshotState = screenshotState) {
                GoogleMap(
                    modifier = Modifier.fillMaxSize(),
                    cameraPositionState = cameraPositionState,
                    properties = MapProperties(
                        isMyLocationEnabled = true,
                        mapType = settings.mapConfig.style,
                    ),
                    uiSettings = MapUiSettings(
                        compassEnabled = false,
                        //rotationGesturesEnabled = false,
                        //scrollGesturesEnabled = false,
                        //scrollGesturesEnabledDuringRotateOrZoom = false,
                        mapToolbarEnabled = false,
                        myLocationButtonEnabled = false,
                        zoomControlsEnabled = false,
                        //zoomGesturesEnabled = false,
                        tiltGesturesEnabled = false,
                        indoorLevelPickerEnabled = false,
                    ),
                ) {
//                if (listPolyline.size != polyData!!.size) {
//                    listPolyline.clear()
                    Polyline(
                        points = polyData!!.flatten(),
                        width = settings.mapConfig.weight.value,
                        color = Color(settings.mapConfig.colorValue)
                    )

//                } else {
//                    polyData?.let { Polyline(
//                        points = it.last(),
//                        width = viewModel.settings.polyLineWidth.value,
//                        color = Color(viewModel.settings.polyLineColor)
//                    ) }
//                }
                }
            }
        }

        Card(
            shape = MaterialTheme.shapes.small,
            backgroundColor = ColorPalette.grey1,
            modifier = Modifier
                .padding(top = 10.dp)
                .padding(bottom = 10.dp)
                .padding(horizontal = 10.dp)
                .fillMaxWidth()
                .weight(.3f),
            elevation = 3.dp,
        ) {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = timeRun.toFullFormatTime(true),
                    style = MaterialTheme.typography.h4,
                    color = TextPalette.light,
                )
                Spacer(modifier = Modifier.height(20.dp))
                Row {
                    ButtonsServices(
                        startEnabled = lastLocation != null,
                        actionServices = { action ->
                            when (action) {
                                TrackingActions.START -> {
                                    TrackingServices.startServicesOrResume(applicationContext)
                                    viewModel.changeAnimation(true)
                                }
                                TrackingActions.RESUME -> {
                                    TrackingServices.pauseServices(applicationContext)
                                    viewModel.changeAnimation(true)
                                }
                                TrackingActions.SAVED -> {
                                    if (polyData?.first()?.isNotEmpty() == true) {
                                        TrackingServices.pauseServices(applicationContext)
                                        viewModel.changeAnimation(false)
                                        bounds = LatLngBounds.builder().includeAll(polyData!!.flatten()).build()
//                                        cameraPositionState.move(
//                                            update = CameraUpdateFactory.newLatLngBounds(bounds!!,50)
//                                        )
//                                        screenshotState.capture()
//                                        changeVisibleDialog(true)
                                    }
                                    else {
                                        TrackingServices.finishServices(applicationContext)
                                        viewModel.changeAnimation(true)
                                    }
                                }
                            }
                        },
                        servicesState = servicesState,
                    )
                }
            }
        }
    }

    if (isDialogShow) {
        SaveDialog(
            session = viewModel.genSession(),
            metricType = settings.metrics,
            imageResult = imageResult,
            hiddenDialog = {
                changeVisibleDialog(false)
            },
            saveSession = {
                viewModel.submitImage(it)
            },
            deleteSession = {
                TrackingServices.finishServices(applicationContext)
                navToHomeScreen()
            },
            cancel = {
                viewModel.changeAnimation(true)
            }
        )
    }
}


