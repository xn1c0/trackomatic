package com.n7labs.trackomatic.ui.screens.main.settings.components

import android.content.Context
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.compose.*
import com.n7labs.trackomatic.domain.model.types.PolyLineWidthType

fun LatLngBounds.Builder.includeAll(points: List<LatLng>): LatLngBounds.Builder {
    points.forEach(::include)
    return this
}

@Composable
fun MapFromConfig(
    style: MapType,
    lineWeight: PolyLineWidthType,
    lineColor: Color,
    modifier: Modifier = Modifier,
    context: Context = LocalContext.current,
) {
    val polyData = remember {
        listOf(
            LatLng(53.3477, -6.2597),
            LatLng(51.5008, -0.1224),
            LatLng(48.8567, 2.3508),
            LatLng(52.5166, 13.3833),
        )
    }

    var mapLoaded by remember { mutableStateOf (false) }

    val cameraPositionState = remember {
        CameraPositionState()
    }

    LaunchedEffect(key1 = mapLoaded) {
        cameraPositionState.move(
            update = CameraUpdateFactory.newLatLngBounds(LatLngBounds.builder().includeAll(polyData).build(),50),
        )
    }

    Box(
        modifier = Modifier
            .height(220.dp)
            .padding(horizontal = 10.dp)
            .clip(shape = MaterialTheme.shapes.small)
    ) {
        GoogleMap(
            cameraPositionState = cameraPositionState,
            properties = MapProperties(
                mapType = style,
            ),
            uiSettings = MapUiSettings(
                compassEnabled = false,
                rotationGesturesEnabled = false,
                scrollGesturesEnabled = false,
                scrollGesturesEnabledDuringRotateOrZoom = false,
                mapToolbarEnabled = false,
                myLocationButtonEnabled = false,
                zoomControlsEnabled = false,
                zoomGesturesEnabled = false,
                tiltGesturesEnabled = false,
                indoorLevelPickerEnabled = false,
            ),
            onMapLoaded = { mapLoaded = !mapLoaded }
        ) {
            Polyline(
                points = polyData,
                width = lineWeight.value,
                color = lineColor
            )
        }
    }
}

