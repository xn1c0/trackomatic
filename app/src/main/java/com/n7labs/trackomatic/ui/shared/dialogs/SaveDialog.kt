package com.n7labs.trackomatic.ui.shared.dialogs

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.n7labs.trackomatic.core.*
import com.n7labs.trackomatic.domain.model.Session
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.smarttoolfactory.screenshot.ImageResult

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SaveDialog(
    session: Session,
    metricType: MetricType,
    imageResult: ImageResult,
    hiddenDialog: () -> Unit,
    saveSession: (ImageBitmap) -> Unit,
    deleteSession: () -> Unit,
    cancel: () -> Unit
) {
    Dialog(
        properties = DialogProperties(
            usePlatformDefaultWidth = false,
            dismissOnBackPress = false,
            dismissOnClickOutside = false
        ),
        onDismissRequest = hiddenDialog,
    ) {

        var cancelBtn by remember {
            mutableStateOf(true)
        }

        var deleteBtn by remember {
            mutableStateOf(true)
        }

        var saveBtn by remember {
            mutableStateOf(false)
        }

        LaunchedEffect(key1 = imageResult){
            if (imageResult is ImageResult.Success) {
                saveBtn = true
            }
        }

        Surface(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 10.dp)
                        .padding(horizontal = 10.dp)
                        .clip(MaterialTheme.shapes.small)
                        .weight(1f)
                ) {
                    when (imageResult) {
                        is ImageResult.Success -> {
                            Image(
                                bitmap = imageResult.data.asImageBitmap(),
                                contentDescription = null,
                                contentScale = ContentScale.FillBounds
                            )
                        }
                        else -> {
                            ProgressBar()
                        }
                    }
                }
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .weight(.7f)
                ) {
                    Card(
                        shape = MaterialTheme.shapes.small,
                        backgroundColor = ColorPalette.grey1,
                        modifier = Modifier
                            .padding(all = 10.dp)
                            .fillMaxSize(),
                        elevation = 3.dp,
                    ) {
                        Column {
                            TitleStatistics(
                                fontSizeTitle = 20.sp,
                            )
                            InfoSession(
                                session = session,
                                modifier = Modifier.padding(10.dp),
                                metricType = metricType,
                                fontSize = 15.sp
                            )
                            Row(
                                modifier = Modifier
                                    .padding(top = 20.dp)
                                    .fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Button(
                                    enabled = deleteBtn,
                                    onClick = {
                                        deleteSession()
                                        hiddenDialog()
                                    }
                                ) {
                                    Text(text = "Delete")
                                }
                                Spacer(modifier = Modifier.width(20.dp))
                                OutlinedButton(
                                    enabled = cancelBtn,
                                    onClick = {
                                        cancel()
                                        hiddenDialog()
                                    }
                                ) {
                                    Text(text = "Cancel")
                                }
                                Spacer(modifier = Modifier.width(20.dp))
                                Button(
                                    enabled = saveBtn,
                                    onClick = {
                                        cancelBtn = false
                                        deleteBtn = false
                                        saveBtn = false
                                        saveSession((imageResult as ImageResult.Success).data.asImageBitmap())
                                    }
                                ) {
                                    Text(text = "Save")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun TitleStatistics(
    fontSizeTitle: TextUnit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(5.dp),
            text = "Session Recap",
            style = MaterialTheme.typography.h5.copy(fontSize = fontSizeTitle),
            color = ColorPalette.Blue.medium
        )
    }
}

@Composable
private fun InfoSession(
    session: Session,
    fontSize: TextUnit,
    metricType: MetricType,
    modifier: Modifier = Modifier,
    context: Context = LocalContext.current
) {
    val date = remember { session.timestamp.toDateFormat() }
    val timeDay = remember { session.timestamp.toDateOnlyTime() }
    val speed = remember { session.avgSpeedInMeters.toAVGSpeed(metricType) }
    val distance = remember { session.distanceInMeters.toMeters(metricType) }
    val calories = remember { session.caloriesBurned.toCaloriesBurned(metricType) }
    val timeRun = remember { session.timeRunInMillis.toFullFormatTime(true) }

    Column(modifier = modifier.verticalScroll(rememberScrollState())) {
        TitleAndInfo("Date", date, fontSize)
        TitleAndInfo("Time", timeDay, fontSize)
        TitleAndInfo("Duration", timeRun, fontSize)
        TitleAndInfo("Distance", distance, fontSize)
        TitleAndInfo("Average speed", speed, fontSize)
        TitleAndInfo("Calories", calories, fontSize)
    }

}

@Composable
private fun TitleAndInfo(
    info: String,
    title: String,
    fontSize: TextUnit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = info,
            style = MaterialTheme.typography.body1.copy(fontSize = fontSize)
        )
        Spacer(modifier = Modifier.width(10.dp))
        Text(
            text = title,
            style = MaterialTheme.typography.body1.copy(fontSize = fontSize)
        )
    }
}