package com.n7labs.trackomatic.ui.screens.main.profile.components

import android.util.Log
import androidx.compose.runtime.Composable
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.Sessions
import com.n7labs.trackomatic.ui.screens.main.profile.ProfileViewModel

@Composable
fun Sessions(
    viewModel: ProfileViewModel,
    graphSessions: @Composable (sessions: Sessions) -> Unit
) {
    when(val response = viewModel.sessionsResponse) {
        is Response.Loading -> {}
        is Response.Success ->  {
            if (response.data != null)
                graphSessions(response.data)
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }
}