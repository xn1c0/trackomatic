package com.n7labs.trackomatic.ui.shared.text

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.FontStyle
import com.n7labs.trackomatic.ui.theme.TextPalette

@Composable
fun FormText(
    text: String,
    fontStyle: FontStyle? = null,
) {
    Text(
        text = text,
        fontStyle = fontStyle,
        color = TextPalette.medium,
    )
}