package com.n7labs.trackomatic.ui.screens.main.profile

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.ui.screens.main.profile.components.GraphSessions
import com.n7labs.trackomatic.ui.screens.main.profile.components.Sessions
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar
import com.n7labs.trackomatic.ui.theme.ColorPalette

@RequiresApi(Build.VERSION_CODES.N)
@Composable
@ExperimentalCoilApi
fun ProfileScreen(
    viewModel: ProfileViewModel
) {

    val settings by remember { mutableStateOf(Settings()) }

    when(val response = viewModel.settingsResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {
            if (response.data != null) {
                settings.mapConfig = response.data.mapConfig
                settings.metrics = response.data.metrics
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    Column(
        modifier = Modifier.padding(all = 10.dp)
    ) {
        Card(
            shape = MaterialTheme.shapes.small,
            backgroundColor = ColorPalette.grey1,
            modifier = Modifier.fillMaxWidth(),
            elevation = 3.dp,
        ) {
            Column(
                modifier = Modifier.padding(vertical = 20.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Sessions(
                    viewModel = viewModel,
                    graphSessions = { sessions ->
                        GraphSessions(
                            sessions = sessions,
                            metricType = settings.metrics,
                        )
                    }
                )
            }

        }
    }
}


