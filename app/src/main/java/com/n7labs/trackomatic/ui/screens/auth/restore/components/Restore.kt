package com.n7labs.trackomatic.ui.screens.auth.restore.components

import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.ui.screens.auth.restore.RestoreViewModel
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar

@Composable
fun Restore(
    viewModel: RestoreViewModel,
    navToSignInScreen: () -> Unit,
) {
    when (val response = viewModel.restoreResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data == true) {
                Toast.makeText(
                    LocalContext.current,
                    "Reset instructions sent to your inbox",
                    Toast.LENGTH_SHORT
                ).show()
                LaunchedEffect(key1 = response) {
                    navToSignInScreen()
                }
            }
        }
        is Response.Failure -> {
            Log.d("AUTH", response.e.toString())
            Toast.makeText(
                LocalContext.current,
                response.e.toString().substringAfter(": "),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}