package com.n7labs.trackomatic.ui.screens.init

import android.view.animation.OvershootInterpolator
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.ui.theme.ColorPalette
import kotlinx.coroutines.delay

@Composable
fun InitScreen(
    viewModel: InitViewModel,
    navToSignInScreen: () -> Unit,
    navToHomeScreen:() -> Unit
) {

    val scale = remember {
        androidx.compose.animation.core.Animatable(0.0f)
    }

    LaunchedEffect(key1 = true) {
        scale.animateTo(
            targetValue = 0.7f,
            animationSpec = tween(800, easing = {
                OvershootInterpolator(4f).getInterpolation(it)
            })
        )
        delay(1500)
        if (viewModel.isVerified) {
            navToHomeScreen()
        } else {
            navToSignInScreen()
        }
    }

    Surface(
        Modifier.fillMaxSize(),
        color = ColorPalette.Blue.medium
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_run),
            contentDescription = "",
            alignment = Alignment.Center, modifier = Modifier
                .fillMaxSize()
                .padding(40.dp)
                .scale(scale.value)
        )
    }
}