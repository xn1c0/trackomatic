package com.n7labs.trackomatic.ui.screens.init

import androidx.lifecycle.ViewModel
import com.n7labs.trackomatic.domain.usecase.auth.IsUserEmailVerified
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InitViewModel @Inject constructor(
    private val isUserVerified: IsUserEmailVerified,
): ViewModel() {

    val isVerified get() = isUserVerified()

}