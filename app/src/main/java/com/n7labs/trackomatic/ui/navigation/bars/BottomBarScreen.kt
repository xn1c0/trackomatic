package com.n7labs.trackomatic.ui.navigation.bars

import androidx.compose.animation.core.SpringSpec
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.BottomNavigation
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.n7labs.trackomatic.ui.navigation.graphs.Graph
import com.n7labs.trackomatic.ui.theme.ColorPalette

@Composable
fun BottomBarScreen(navController: NavHostController) {
    BottomMenu(navController)
}

@Composable
fun BottomMenu(
    navController: NavHostController,
    modifier: Modifier = Modifier,
    activeHighlightColor: Color = ColorPalette.Blue.light,
    activeTextColor: Color = Color.White,
    inactiveTextColor: Color = Color.White,
) {

    val barContent = listOf(
        BottomBarContent.Home,
        BottomBarContent.ToM,
        BottomBarContent.Profile,
        BottomBarContent.Settings,
    )
    /**
     * Choose when to show the bottom bar based on current destination
     * Subscribe to navBackStackEntry, required to get current route!
     */
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination
    val bottomBarDestination = barContent.any { it.route == currentDestination?.route }
    if (bottomBarDestination) {
        Box {
            BottomNavigation(
                backgroundColor = ColorPalette.Blue.medium,
                modifier = modifier
                    .fillMaxWidth()
            ) {
                barContent.forEach { content ->
                    BottomMenuContent(
                        content = content,
                        navController = navController,
                        isSelected = currentDestination?.hierarchy?.any {
                            it.route == content.route
                        } == true,
                        activeHighlightColor = activeHighlightColor,
                        activeTextColor = activeTextColor,
                        inactiveTextColor = inactiveTextColor
                    )
                }
            }
        }
    }
}

@Composable
fun BottomMenuContent(
    content: BottomBarContent,
    navController: NavHostController,
    isSelected: Boolean,
    activeHighlightColor: Color = ColorPalette.orange,
    activeTextColor: Color = Color.White,
    inactiveTextColor: Color = Color.White,
) {
    val top by animateDpAsState(
        targetValue = if (isSelected) 0.dp else 56.dp,
        animationSpec = SpringSpec(dampingRatio = 0.5f, stiffness = 200f)
    )
    Box(
        modifier = Modifier
            .height(56.dp)
            .padding(start = 30.dp, end = 30.dp)
            .clickable(
                indication = null,
                interactionSource = remember { MutableInteractionSource() } // This is mandatory
            ) {
                if(content.route != navController.currentDestination!!.route) {
                    navController.navigate(content.route) {
                        popUpTo(Graph.MAIN) {
                            inclusive = true
                        }
                        launchSingleTop = true
                    }
                }
            },
        contentAlignment = Alignment.Center,
    ) {
        Icon(
            imageVector = content.icon,
            tint = if (isSelected) activeTextColor else inactiveTextColor,
            contentDescription = null,
            modifier = Modifier
                .height(56.dp)
                .width(26.dp)
                .offset(y = top)
        )
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .height(56.dp)
                .offset(y = top - 56.dp)
        ) {
            Text(
                text = content.title,
                fontSize = 13.sp,
                color = if(isSelected) activeTextColor else inactiveTextColor,
            )
        }
    }
}