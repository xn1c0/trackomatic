package com.n7labs.trackomatic.ui.shared.text

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.n7labs.trackomatic.ui.theme.ColorPalette

@Composable
fun FormTextErrorMessage(
    msg: String?
) {
    if (msg != null) {
        Text(
            text = msg,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 1.dp),
            fontSize = 12.sp,
            color = ColorPalette.orange
        )
    }
}