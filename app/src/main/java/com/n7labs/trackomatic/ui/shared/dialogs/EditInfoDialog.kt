package com.n7labs.trackomatic.ui.shared.dialogs

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.core.*
import com.n7labs.trackomatic.domain.model.types.GenderType
import com.n7labs.trackomatic.domain.states.SettingsUIState
import com.n7labs.trackomatic.ui.shared.fields.PlainField
import com.n7labs.trackomatic.ui.shared.fields.SelectOptionConfig
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun EditInfoDialog(
    settingsUIState: SettingsUIState,
    changeAge: (String) -> Unit,
    changeHeight: (String) -> Unit,
    changeWeight: (String) -> Unit,
    changeGender: (GenderType) -> Unit,
    changedFocusAge: () -> Unit,
    changedFocusHeight: () -> Unit,
    changedFocusWeight: () -> Unit,
    hiddenDialog: () -> Unit,
    saveProfile: () -> Unit,
) {

    Dialog(
        properties = DialogProperties(
            usePlatformDefaultWidth = false,
            dismissOnBackPress = false,
            dismissOnClickOutside = false
        ),
        onDismissRequest = hiddenDialog,
    ) {

        val localFocus = LocalFocusManager.current

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
        ) {
            Card(
                shape = MaterialTheme.shapes.small,
                backgroundColor = ColorPalette.grey1,
                modifier = Modifier.fillMaxWidth(),
                elevation = 3.dp,
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.height(10.dp))
                    Text(
                        text = "Edit profile info's",
                        color = TextPalette.light,
                        style = MaterialTheme.typography.h6,
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    )
                    PlainField(
                        initial = settingsUIState.age,
                        label = "Age",
                        painter = painterResource(id = R.drawable.ic_celebration),
                        onTextChanged = {
                            changeAge(it)
                        },
                        keyboardType = KeyboardType.Decimal,
                        errorMessage = settingsUIState.ageError,
                        imeAction = ImeAction.Next,
                        onNext = {
                            changedFocusAge()
                            localFocus.moveFocus(FocusDirection.Down)
                        },
                        onDone = {},
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            unfocusedBorderColor = ColorPalette.Blue.medium,
                            focusedBorderColor = ColorPalette.Blue.medium,
                            textColor = TextPalette.light,
                            focusedLabelColor = ColorPalette.Blue.medium,
                            errorBorderColor = ColorPalette.orange,
                            errorCursorColor = ColorPalette.orange,
                        )
                    )
                    PlainField(
                        initial = settingsUIState.height,
                        label = "Height",
                        painter = painterResource(id = R.drawable.ic_height),
                        onTextChanged = {
                            changeHeight(it)
                        },
                        keyboardType = KeyboardType.Decimal,
                        errorMessage = settingsUIState.heightError,
                        imeAction = ImeAction.Next,
                        onNext = {
                            changedFocusHeight()
                            localFocus.moveFocus(FocusDirection.Down)
                        },
                        onDone = {},
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            unfocusedBorderColor = ColorPalette.Blue.medium,
                            focusedBorderColor = ColorPalette.Blue.medium,
                            textColor = TextPalette.light,
                            focusedLabelColor = ColorPalette.Blue.medium,
                            errorBorderColor = ColorPalette.orange,
                            errorCursorColor = ColorPalette.orange,
                        )
                    )
                    PlainField(
                        initial = settingsUIState.weight,
                        label = "Weight",
                        painter = painterResource(id = R.drawable.ic_weight),
                        onTextChanged = {
                            changeWeight(it)
                        },
                        keyboardType = KeyboardType.Decimal,
                        errorMessage = settingsUIState.weightError,
                        imeAction = ImeAction.Done,
                        onNext = {},
                        onDone = {
                            changedFocusWeight()
                            localFocus.clearFocus()
                        },
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            unfocusedBorderColor = ColorPalette.Blue.medium,
                            focusedBorderColor = ColorPalette.Blue.medium,
                            textColor = TextPalette.light,
                            focusedLabelColor = ColorPalette.Blue.medium,
                            errorBorderColor = ColorPalette.orange,
                            errorCursorColor = ColorPalette.orange,
                        )
                    )
                    Column(
                        modifier = Modifier
                            .padding(top = 15.dp)
                            .padding(horizontal = 10.dp)
                    ) {
                        SelectGender(
                            currentGenderType = settingsUIState.gender,
                            changeGender = {
                                changeGender(it)
                            }
                        )
                    }
                    Row(
                        modifier = Modifier
                            .padding(top = 20.dp)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        OutlinedButton(
                            onClick = {
                                hiddenDialog()
                            }
                        ) {
                            Text(text = "Quit")
                        }
                        Spacer(modifier = Modifier.width(20.dp))
                        Button(
                            onClick = {
                                saveProfile()
                            }
                        ) {
                            Text(text = "Save")
                        }
                    }
                    Spacer(modifier = Modifier.height(10.dp))
                }
            }
        }
    }
}



@Composable
private fun SelectGender(
    currentGenderType: GenderType,
    changeGender: (GenderType) -> Unit,
) {
    val listGenders = remember { GenderType.values().toList() }

    SelectOptionConfig(
        textField = "Gender",
        selected = currentGenderType.toString(),
        listItems = listGenders,
        listNamed = listGenders.map { it.name },
        onChange = changeGender
    )
}