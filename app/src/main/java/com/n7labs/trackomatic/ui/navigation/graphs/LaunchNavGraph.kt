package com.n7labs.trackomatic.ui.navigation.graphs

import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.n7labs.trackomatic.ui.screens.init.InitScreen

fun NavGraphBuilder.launchNavGraph(
    navController: NavHostController
) {
    navigation(
        route = Graph.LAUNCH,
        startDestination = LaunchScreen.Init.route
    ) {
        composable(route = LaunchScreen.Init.route) {
            InitScreen(
                viewModel = hiltViewModel(),
                navToSignInScreen = {
                    navController.navigate(Graph.AUTHENTICATION) {
                        popUpTo(Graph.LAUNCH) {
                            inclusive = true
                        }
                    }
                },
                navToHomeScreen = {
                    navController.navigate(Graph.MAIN) {
                        popUpTo(Graph.LAUNCH) {
                            inclusive = true
                        }
                    }
                },
            )
        }
    }
}

sealed class LaunchScreen(val route: String) {
    object Init : AuthScreen(route = "init")
}