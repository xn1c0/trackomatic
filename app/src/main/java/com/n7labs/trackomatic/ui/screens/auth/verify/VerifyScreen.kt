package com.n7labs.trackomatic.ui.screens.auth.verify

import android.os.Build
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.ImageLoader
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberAsyncImagePainter
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.request.ImageRequest
import coil.size.Size
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.domain.events.VerifyUIEvent
import com.n7labs.trackomatic.ui.screens.auth.verify.components.IsEmailVerified
import com.n7labs.trackomatic.ui.screens.auth.verify.components.SendEmailVerification
import com.n7labs.trackomatic.ui.screens.auth.verify.components.SignOut
import com.n7labs.trackomatic.ui.shared.buttons.FormTextButton
import com.n7labs.trackomatic.ui.shared.buttons.SubmitButton
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.Nova
import com.n7labs.trackomatic.ui.theme.TextPalette
import com.n7labs.trackomatic.ui.theme.TrackOMaticTheme

@Composable
@ExperimentalCoilApi
fun VerifyScreen(
    viewModel: VerifyViewModel,
    navToSignInScreen: () -> Unit,
    navToHomeScreen:() -> Unit
) {

    val coroutineScope = rememberCoroutineScope()

    /**
     * Used to handle recomposition based on responses
     */
    IsEmailVerified(viewModel = viewModel, navToHomeScreen = navToHomeScreen)
    SendEmailVerification(viewModel = viewModel)
    SignOut(viewModel = viewModel, navToSignInScreen = navToSignInScreen)

    /**
     * Beginning of composable UI
     */
    val imageLoader = ImageLoader.Builder(LocalContext.current)
        .components {
            if (Build.VERSION.SDK_INT >= 28) {
                add(ImageDecoderDecoder.Factory())
            } else {
                add(GifDecoder.Factory())
            }
        }
        .build()

    TrackOMaticTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = ColorPalette.Blue.medium
        ) {
            Box(
                contentAlignment = Alignment.TopCenter
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(
                        modifier = Modifier.fillMaxWidth(),
                        contentScale = ContentScale.Crop,
                        painter = rememberAsyncImagePainter(
                            ImageRequest.Builder(LocalContext.current)
                                .data(data = R.drawable.verify_illustration_light)
                                .apply(block = fun ImageRequest.Builder.() {
                                    size(Size.ORIGINAL)
                                }).build(), imageLoader = imageLoader
                        ),
                        contentDescription = "restore_illustration",
                    )
                }
            }
            Box(
                contentAlignment = Alignment.BottomCenter
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "Verify your email",
                        color = Color.White,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                        style = TextStyle(
                            fontSize = 30.sp,
                            shadow = Shadow(
                                color = Color.Black,
                                offset = Offset(3.0f, 3.0f),
                                blurRadius = 5f
                            )
                        ),
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold,
                        fontFamily = Nova,
                    )
                    Card(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 20.dp)
                            .padding(top = 20.dp),
                        backgroundColor = Color.White,
                        elevation = 0.dp
                    ) {
                        Column(
                            modifier = Modifier.padding(vertical = 20.dp),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Text(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(horizontal = 20.dp)
                                    .padding(top = 10.dp),
                                text = "You have not verified your email. Please check your inbox, " +
                                        "if you didn't receive the verification please click on the " +
                                        "button down below.\n" +
                                        "And don't forget to check your spam inbox!",
                                fontFamily = Nova,
                                color = TextPalette.light,
                                textAlign = TextAlign.Center,
                                fontWeight = FontWeight.ExtraBold,
                                fontSize = 16.sp
                            )
                            SubmitButton(
                                onClick = { viewModel.onEvent(VerifyUIEvent.SendVerification) },
                                text = "Send new verification",
                            )
                            Spacer(modifier = Modifier.padding(top = 10.dp))
                        }
                    }
                    FormTextButton(
                        modifier = Modifier.padding(top = 30.dp),
                        onClick = { viewModel.onEvent(VerifyUIEvent.SignOut) },
                        text = "Not you? Sign out!",
                        fontSize = 14.sp,
                        color = TextPalette.ultralight
                    )
                }
            }
        }
    }
}