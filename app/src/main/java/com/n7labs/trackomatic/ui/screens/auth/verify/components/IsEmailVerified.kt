package com.n7labs.trackomatic.ui.screens.auth.verify.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import com.n7labs.trackomatic.ui.screens.auth.verify.VerifyViewModel

@Composable
fun IsEmailVerified(
    viewModel: VerifyViewModel,
    navToHomeScreen: () -> Unit
) {
    LaunchedEffect(Unit) {
        if(viewModel.isVerified) {
            navToHomeScreen()
        }
    }
}