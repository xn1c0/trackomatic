package com.n7labs.trackomatic.ui.screens.main.home.components

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.n7labs.trackomatic.core.*
import com.n7labs.trackomatic.domain.model.Session
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette

@Composable
fun SessionCard(
    session: Session,
    metricType: MetricType,
    showDialogStatistics: (session: Session) -> Unit,
) {
    Card(
        shape = MaterialTheme.shapes.small,
        backgroundColor = ColorPalette.grey1,
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                showDialogStatistics(session)
            },
        elevation = 3.dp,
    ) {
        Row(modifier = Modifier
            .padding(10.dp)
            .height(150.dp)) {
            Column(modifier = Modifier.fillMaxWidth(.4f)) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(session.uri)
                        .crossfade(true)
                        .build(),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .clip(MaterialTheme.shapes.medium)
                )
            }
            Spacer(modifier = Modifier.width(30.dp))
            Column(modifier = Modifier.fillMaxWidth()) {
                InfoSession(session = session, metricType = metricType)
            }
        }
    }
}

@Composable
private fun InfoSession(
    session: Session,
    metricType: MetricType,
    modifier: Modifier = Modifier,
    context: Context = LocalContext.current
) {
    val date = remember { session.timestamp.toDateFormat() }
    val timeDay = remember { session.timestamp.toDateOnlyTime() }
    val timeRun = remember { session.timeRunInMillis.toFullFormatTime(false) }
    val distance = remember { session.distanceInMeters.toMeters(metricType) }
    val calories = remember { session.caloriesBurned.toCaloriesBurned(metricType) }
    val speed = remember { session.avgSpeedInMeters.toAVGSpeed(metricType) }

    Row(
        modifier = modifier
            .padding(vertical = 10.dp)
            .fillMaxWidth()
    ) {
        Column(verticalArrangement = Arrangement.SpaceAround, modifier = Modifier.fillMaxHeight()) {
            DetailsSessionText(text = "Date", ColorPalette.Blue.medium)
            DetailsSessionText(text = "Time", ColorPalette.Blue.medium)
            DetailsSessionText(text = "Duration", ColorPalette.Blue.medium)
            DetailsSessionText(text = "Distance", ColorPalette.Blue.medium)
            DetailsSessionText(text = "Speed", ColorPalette.Blue.medium)
            DetailsSessionText(text = "Calories", ColorPalette.Blue.medium)
        }
        Spacer(modifier = Modifier.width(10.dp))
        Column(verticalArrangement = Arrangement.SpaceAround, modifier = Modifier.fillMaxHeight()) {
            DetailsSessionText(text = date, TextPalette.light)
            DetailsSessionText(text = timeDay, TextPalette.light)
            DetailsSessionText(text = timeRun, TextPalette.light)
            DetailsSessionText(text = distance, TextPalette.light)
            DetailsSessionText(text = speed, TextPalette.light)
            DetailsSessionText(text = calories, TextPalette.light)
        }
    }
}

@Composable
private fun DetailsSessionText(
    text: String,
    color: Color
) {
    Text(
        text = text,
        style = MaterialTheme.typography.caption,
        color = color,
        fontSize = 14.sp,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
    )
}