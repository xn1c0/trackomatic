package com.n7labs.trackomatic.ui.screens.main.settings

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.n7labs.trackomatic.domain.events.SettingsUIEvent
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.domain.model.UserProfile
import com.n7labs.trackomatic.domain.repository.*
import com.n7labs.trackomatic.domain.states.SettingsUIState
import com.n7labs.trackomatic.domain.usecase.auth.RevokeGoogleAccess
import com.n7labs.trackomatic.domain.usecase.auth.SignOut
import com.n7labs.trackomatic.domain.usecase.profile.GetProfile
import com.n7labs.trackomatic.domain.usecase.profile.SaveProfile
import com.n7labs.trackomatic.domain.usecase.setting.GetSettings
import com.n7labs.trackomatic.domain.usecase.setting.SaveSettings
import com.n7labs.trackomatic.domain.usecase.validatable.ValidateAge
import com.n7labs.trackomatic.domain.usecase.validatable.ValidateWH
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val signOut: SignOut,
    private val revokeGoogleAccess: RevokeGoogleAccess,
    private val saveSettings: SaveSettings,
    private val getSettings: GetSettings,
    private val getProfile: GetProfile,
    private val saveProfile: SaveProfile,
    private val validateAge: ValidateAge,
    private val validateWH: ValidateWH
): ViewModel() {

    var signOutResponse by mutableStateOf<SignOutResponse>(Response.Success(false))
        private set
    var revokeAccessResponse by mutableStateOf<RevokeAccessResponse>(Response.Success(false))
        private set
    var settingsResponse by mutableStateOf<SettingsResponse>(Response.Loading)
        private set
    var settingsSaveResponse by mutableStateOf<SettingsSaveResponse>(Response.Success(false))
        private set
    var profileResponse by mutableStateOf<ProfileResponse>(Response.Loading)
        private set
    var profileSaveResponse by mutableStateOf<ProfileSaveResponse>(Response.Success(false))
        private set

    private val _settingsUIState = MutableStateFlow(SettingsUIState())
    val settingsUIState get() = _settingsUIState.asStateFlow()

    private val _profileState = MutableStateFlow(UserProfile())
    val profileState get() = _profileState.asStateFlow()

    private val _settingsState = MutableStateFlow(Settings())
    val settingsState get() = _settingsState.asStateFlow()

    init {
        loadSetting()
        loadProfile()
    }

    // Keeps updating data on change see profile repo
    private fun loadSetting() = viewModelScope.launch {
        getSettings().collect { response ->
            settingsResponse = response
        }
    }

    private fun loadProfile() = viewModelScope.launch {
        getProfile().collect { response ->
            profileResponse = response
        }
    }

    fun onEvent(event: SettingsUIEvent) = when(event) {
        is SettingsUIEvent.AgeChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(age = event.age)
        }
        is SettingsUIEvent.HeightChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(height = event.height)
        }
        is SettingsUIEvent.WeightChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(weight = event.weight)
        }
        is SettingsUIEvent.GenderChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(gender = event.gender)
        }
        is SettingsUIEvent.LineColorChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(
                lineColor = event.lineColor
            )
            sSettings(settingsState.value.copy(
                mapConfig = settingsState.value.mapConfig.copy(
                    colorValue = event.lineColor.toArgb()
                )
            ))
        }
        is SettingsUIEvent.LineWeightChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(
                lineWeight = event.lineWeight
            )
            sSettings(settingsState.value.copy(
                mapConfig = settingsState.value.mapConfig.copy(
                    weight = event.lineWeight
                )
            ))
        }
        is SettingsUIEvent.MetricsChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(metrics = event.metrics)
            sSettings(settingsState.value.copy(metrics = event.metrics))
        }
        is SettingsUIEvent.StyleChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(style = event.style)
            sSettings(settingsState.value.copy(
                mapConfig = settingsState.value.mapConfig.copy(
                    style = event.style
                )
            ))
        }
        is SettingsUIEvent.UserIDChanged -> {
            _settingsUIState.value = settingsUIState.value.copy(userID = event.userID)
            sSettings(settingsState.value.copy(userID = event.userID))
        }
        is SettingsUIEvent.AgeFocusChanged -> {
            val result = validateAge(body = _settingsUIState.value.age)
            _settingsUIState.value = settingsUIState.value.copy(
                ageError = result.errorMessage
            )
        }
        is SettingsUIEvent.HeightFocusChanged -> {
            val result = validateWH(body = _settingsUIState.value.height)
            _settingsUIState.value = settingsUIState.value.copy(
                heightError = result.errorMessage
            )
        }
        is SettingsUIEvent.WeightFocusChanged -> {
            val result = validateWH(body = _settingsUIState.value.weight)
            _settingsUIState.value = settingsUIState.value.copy(
                weightError = result.errorMessage
            )
        }
        is SettingsUIEvent.SignOut -> {
            optOut()
        }
        is SettingsUIEvent.RevokeGoogleAccess -> {
            revokeAccess()
        }
        is SettingsUIEvent.DeleteAccount -> {
            TODO()
        }
        is SettingsUIEvent.ProfileChanged -> {
            _profileState.value = profileState.value.copy(
                age = event.userProfile.age,
                height = event.userProfile.height,
                weight = event.userProfile.weight,
                gender = event.userProfile.gender
            )
//            _settingsUIState.value = settingsUIState.value.copy(
//                age = profileState.value.age.toString(),
//                height = profileState.value.height.toString(),
//                weight = profileState.value.weight.toString(),
//                gender = profileState.value.gender
//            )
        }
        is SettingsUIEvent.SettingsChanged -> {
            _settingsState.value = settingsState.value.copy(
                mapConfig = event.settings.mapConfig,
                metrics =  event.settings.metrics,
                userID = event.settings.userID
            )
            _settingsUIState.value = settingsUIState.value.copy(
                style = event.settings.mapConfig.style,
                lineWeight = event.settings.mapConfig.weight,
                lineColor = Color(event.settings.mapConfig.colorValue),
                metrics = event.settings.metrics,
                userID = event.settings.userID,
            )
        }
        is SettingsUIEvent.ResetProfileState -> {
            _settingsUIState.value = settingsUIState.value.copy(
                age = _profileState.value.age.toString(),
                height = _profileState.value.height.toString(),
                weight = _profileState.value.weight.toString(),
                gender = _profileState.value.gender,
                ageError = null,
                heightError = null,
                weightError = null
            )
        }
        is SettingsUIEvent.SubmitProfile -> {
            submitProfile()
        }
        SettingsUIEvent.ResetProfileSaveResponse -> {
            profileSaveResponse = Response.Success(false)
        }
    }

    private fun submitProfile() {
        val ageResult = validateAge(body = _settingsUIState.value.age)
        val heightResult = validateWH(body = _settingsUIState.value.height)
        val weightResult = validateWH(body = _settingsUIState.value.weight)

        val hasError = listOf(
            ageResult,
            heightResult,
            weightResult
        ).any {
            !it.isSuccessful
        }

        _settingsUIState.value = settingsUIState.value.copy(
            ageError = ageResult.errorMessage,
            heightError = heightResult.errorMessage,
            weightError = weightResult.errorMessage,
        )

        val current = _profileState.value
        val other = settingsUIState.value

        val e1 = current.age.toString() == other.age
        val e2 = current.height.toString() == other.height
        val e3 = current.weight.toString() == other.weight
        val e4 = current.gender == other.gender

        val isEqual = e1 && e2 && e3 && e4

        if(!hasError && !isEqual) {
            sProfile(
                profileState.value.copy(
                    age = settingsUIState.value.age.toInt(),
                    height = settingsUIState.value.height.toFloat(),
                    weight = settingsUIState.value.weight.toFloat(),
                    gender = settingsUIState.value.gender
                )
            )
        }
    }

    private fun sSettings(settings: Settings) {
        viewModelScope.launch {
            settingsSaveResponse = Response.Loading
            settingsSaveResponse = saveSettings(settings)
        }
    }
    private fun sProfile(userProfile: UserProfile) {
        viewModelScope.launch {
            profileSaveResponse = Response.Loading
            profileSaveResponse = saveProfile(userProfile)
        }
    }
    private fun optOut() {
        viewModelScope.launch {
            signOutResponse = Response.Loading
            signOutResponse = signOut()
        }
    }
    private fun revokeAccess() {
        viewModelScope.launch {
            revokeAccessResponse = Response.Loading
            revokeAccessResponse = revokeGoogleAccess()
        }
    }
}