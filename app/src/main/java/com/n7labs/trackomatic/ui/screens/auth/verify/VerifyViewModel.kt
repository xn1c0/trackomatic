package com.n7labs.trackomatic.ui.screens.auth.verify

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.n7labs.trackomatic.domain.events.VerifyUIEvent
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.SendEmailVerificationResponse
import com.n7labs.trackomatic.domain.repository.SignOutResponse
import com.n7labs.trackomatic.domain.usecase.auth.IsUserEmailVerified
import com.n7labs.trackomatic.domain.usecase.auth.SendEmailVerification
import com.n7labs.trackomatic.domain.usecase.auth.SignOut
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VerifyViewModel @Inject constructor(
    private val isUserVerified: IsUserEmailVerified,
    private val sendEmailVerification: SendEmailVerification,
    private val signOut: SignOut
): ViewModel() {

    val isVerified get() = isUserVerified()

    var signOutResponse by mutableStateOf<SignOutResponse>(Response.Success(false))
        private set

    var sendEmailVerificationResponse by mutableStateOf<SendEmailVerificationResponse>(Response.Success(false))
        private set

    fun onEvent(event: VerifyUIEvent) = when(event) {
        is VerifyUIEvent.SignOut -> {
            optOut()
        }
        is VerifyUIEvent.SendVerification -> {
            sendEmail()
        }
    }

    private fun sendEmail() {
        viewModelScope.launch {
            sendEmailVerificationResponse = Response.Loading
            sendEmailVerificationResponse = sendEmailVerification()
        }
    }

    private fun optOut() {
        viewModelScope.launch {
            signOutResponse = Response.Loading
            signOutResponse = signOut()
        }
    }

}