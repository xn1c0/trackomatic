package com.n7labs.trackomatic.ui.shared.text

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.n7labs.trackomatic.ui.theme.TextPalette

@Composable
fun TitleConfig(
    text: String,
    modifier: Modifier=Modifier
) {
    Text(text = text,
        fontWeight = FontWeight.W500,
        color = TextPalette.medium,
        fontSize = 16.sp,
        modifier = modifier.padding(vertical = 10.dp, horizontal = 20.dp))
}
