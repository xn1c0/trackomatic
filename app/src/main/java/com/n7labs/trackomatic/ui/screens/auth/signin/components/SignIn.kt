package com.n7labs.trackomatic.ui.screens.auth.signin.components

import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.ui.screens.auth.signin.SignInViewModel
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar

@Composable
fun SignIn(
    viewModel: SignInViewModel,
    navToVerifyScreen: () -> Unit,
) {
    when (val response = viewModel.signInResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data == true) {
                Toast.makeText(
                    LocalContext.current,
                    "Sign in completed!",
                    Toast.LENGTH_SHORT
                ).show()
                LaunchedEffect(key1 = response) {
                    navToVerifyScreen()
                }
            }
        }
        is Response.Failure -> {
            Log.d("AUTH", response.e.toString())
            Toast.makeText(
                LocalContext.current,
                response.e.toString().substringAfter(": "),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}