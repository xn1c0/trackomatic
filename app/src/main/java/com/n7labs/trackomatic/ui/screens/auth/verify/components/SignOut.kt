package com.n7labs.trackomatic.ui.screens.auth.verify.components

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.ui.screens.auth.verify.VerifyViewModel
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar

@Composable
fun SignOut(
    viewModel: VerifyViewModel,
    navToSignInScreen: () -> Unit
) {
    when (val response = viewModel.signOutResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data == true) {
                Toast.makeText(
                    LocalContext.current,
                    "You are signed out!",
                    Toast.LENGTH_SHORT
                ).show()
                LaunchedEffect(key1 = viewModel.signOutResponse) {
                    navToSignInScreen()
                }
            }
        }
        is Response.Failure -> {
            Toast.makeText(
                LocalContext.current,
                response.e.toString().substringAfter(": "),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}