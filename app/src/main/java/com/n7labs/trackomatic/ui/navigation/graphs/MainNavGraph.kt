package com.n7labs.trackomatic.ui.navigation.graphs

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.*
import androidx.navigation.compose.composable
import coil.annotation.ExperimentalCoilApi
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.n7labs.trackomatic.ui.navigation.bars.BottomBarContent
import com.n7labs.trackomatic.ui.screens.init.permission.RequestMultiplePermissions
import com.n7labs.trackomatic.ui.screens.main.home.HomeScreen
import com.n7labs.trackomatic.ui.screens.main.profile.ProfileScreen
import com.n7labs.trackomatic.ui.screens.main.settings.SettingsScreen
import com.n7labs.trackomatic.ui.screens.main.tom.ToMScreen

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalCoilApi::class, ExperimentalPermissionsApi::class)
fun NavGraphBuilder.mainNavGraph(
    navController: NavHostController
) {
    navigation(
        route = Graph.MAIN,
        startDestination = BottomBarContent.Home.route
    ) {
        composable(route = BottomBarContent.Home.route) {
            HomeScreen(
                viewModel = hiltViewModel()
            )
        }
        composable(route = BottomBarContent.ToM.route) {
            RequestMultiplePermissions(
                content = {
                    ToMScreen(
                        viewModel = hiltViewModel(),
                        navToHomeScreen = {
                            navController.popBackStack()
                            navController.navigate(Graph.MAIN) {
                                popUpTo(Graph.MAIN) {
                                    inclusive = true
                                }
                                launchSingleTop = true
                            }
                        }
                    )
                },
                permissions = (listOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION))
            )

        }
        composable(route = BottomBarContent.Profile.route) {
            ProfileScreen(
                viewModel = hiltViewModel()
            )
        }
        composable(route = BottomBarContent.Settings.route) {
            SettingsScreen(
                viewModel = hiltViewModel(),
                navToSignInScreen = {
                    navController.popBackStack()
                    navController.navigate(Graph.AUTHENTICATION) {
                        popUpTo(Graph.AUTHENTICATION) {
                            inclusive = true
                        }
                    }
                }
            )
        }
    }
}