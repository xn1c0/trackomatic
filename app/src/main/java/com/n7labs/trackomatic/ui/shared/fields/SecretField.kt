package com.n7labs.trackomatic.ui.shared.fields

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.input.*
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.ui.shared.icons.FieldLeadingIcon
import com.n7labs.trackomatic.ui.shared.icons.FieldTrailingIcon
import com.n7labs.trackomatic.ui.shared.text.FormTextErrorMessage
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette


@Composable
fun SecretField(
    label: String = "",
    painter: Painter,
    keyboardType: KeyboardType = KeyboardType.Password,
    keyboardCapitalization: KeyboardCapitalization = KeyboardCapitalization.None,
    imeAction: ImeAction = ImeAction.Next,
    errorMessage: String? = null,
    onTextChanged: (String) -> Unit,
    onNext: (KeyboardActionScope) -> Unit,
    onDone: (KeyboardActionScope) -> Unit
) {
    var text by remember { mutableStateOf("") }
    var isHidden by remember { mutableStateOf(false) }
    val hasError = errorMessage != null
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp)
            .padding(horizontal = 20.dp),
    ) {
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            value = text,
            onValueChange = {
                text = it
                onTextChanged(it)
            },
            label = { Text(text = label, color = TextPalette.light) },
            keyboardOptions = KeyboardOptions.Default.copy(
                capitalization = keyboardCapitalization,
                autoCorrect = false,
                keyboardType = keyboardType,
                imeAction = imeAction
            ),
            keyboardActions = KeyboardActions(
                onDone = onDone,
                onNext = onNext
            ),
            isError = errorMessage != null,
            singleLine = true,
            leadingIcon = {
                Row(
                    modifier = Modifier.padding(start = 4.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    IconButton(
                        onClick = { text = "" },
                    ) {
                        FieldLeadingIcon(
                            painter = painter,
                            hasError = hasError
                        )
                    }
                    Spacer(modifier = Modifier
                        .width(1.dp)
                        .height(26.dp)
                        .background(if (hasError) ColorPalette.orange else ColorPalette.Blue.medium)
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                }
            },
            visualTransformation = if(!isHidden) PasswordVisualTransformation() else VisualTransformation.None,
            trailingIcon =  {
                IconButton(onClick = { isHidden = !isHidden }) {
                    FieldTrailingIcon(isHidden = isHidden)
                }
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                unfocusedBorderColor = ColorPalette.Blue.medium,
                focusedBorderColor = ColorPalette.Blue.medium,
                textColor = TextPalette.medium,
                focusedLabelColor = ColorPalette.Blue.medium,
                errorBorderColor = ColorPalette.orange,
                errorCursorColor = ColorPalette.orange,
            )
        )
        FormTextErrorMessage(msg = errorMessage)
    }
}