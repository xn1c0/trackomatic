package com.n7labs.trackomatic.ui.shared.fields

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.ui.shared.icons.FieldLeadingIcon
import com.n7labs.trackomatic.ui.shared.text.FormTextErrorMessage
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette

@Composable
fun PlainField(
    initial: String = "",
    label: String = "",
    painter: Painter,
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors(
        unfocusedBorderColor = ColorPalette.Blue.medium,
        focusedBorderColor = ColorPalette.Blue.medium,
        textColor = TextPalette.medium,
        focusedLabelColor = ColorPalette.Blue.medium,
        errorBorderColor = ColorPalette.orange,
        errorCursorColor = ColorPalette.orange,
    ),
    keyboardType: KeyboardType = KeyboardType.Text,
    keyboardCapitalization: KeyboardCapitalization = KeyboardCapitalization.None,
    imeAction: ImeAction,
    errorMessage: String? = null,
    onTextChanged: (String) -> Unit,
    onNext: (KeyboardActionScope) -> Unit,
    onDone: (KeyboardActionScope) -> Unit
) {
    var text by remember { mutableStateOf(initial) }
    val hasError = errorMessage != null
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp)
            .padding(horizontal = 20.dp)
    ) {
        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth(),
            value = text,
            onValueChange = {
                text = it
                onTextChanged(it)
            },
            label = { Text(text = label, color = TextPalette.light) },
            keyboardOptions = KeyboardOptions.Default.copy(
                capitalization = keyboardCapitalization,
                autoCorrect = false,
                keyboardType = keyboardType,
                imeAction = imeAction
            ),
            keyboardActions = KeyboardActions(
                onDone = onDone,
                onNext = onNext,
            ),
            isError = errorMessage != null,
            singleLine = true,
            leadingIcon = {
                Row(
                    modifier = Modifier.padding(start = 4.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    IconButton(
                        onClick = { text = "" },
                    ) {
                        FieldLeadingIcon(
                            painter = painter,
                            hasError = hasError
                        )
                    }
                    Spacer(modifier = Modifier
                        .width(1.dp)
                        .height(26.dp)
                        .background(if (hasError) ColorPalette.orange else ColorPalette.Blue.medium)
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                }
            },
            colors = colors,
        )
        FormTextErrorMessage(msg = errorMessage)
    }
}