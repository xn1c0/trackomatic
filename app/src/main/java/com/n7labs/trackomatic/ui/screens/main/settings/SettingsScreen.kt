package com.n7labs.trackomatic.ui.screens.main.settings

import android.annotation.SuppressLint
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.annotation.ExperimentalCoilApi
import com.n7labs.trackomatic.data.location.TrackingServices
import com.n7labs.trackomatic.domain.events.SettingsUIEvent
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.ui.screens.main.settings.components.*
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar
import com.n7labs.trackomatic.ui.shared.buttons.SubmitButton
import com.n7labs.trackomatic.ui.shared.dialogs.EditInfoDialog
import com.n7labs.trackomatic.ui.theme.*

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
@ExperimentalCoilApi
fun SettingsScreen(
    viewModel: SettingsViewModel,
    navToSignInScreen: () -> Unit
) {

    val applicationContext = LocalContext.current.applicationContext

    when (val response = viewModel.signOutResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data == true) {
                TrackingServices.finishServices(applicationContext)
                Toast.makeText(
                    LocalContext.current,
                    "You are signed out!",
                    Toast.LENGTH_SHORT
                ).show()
                LaunchedEffect(key1 = viewModel.signOutResponse) {
                    navToSignInScreen()
                }
            }
        }
        is Response.Failure -> {
            Toast.makeText(
                LocalContext.current,
                response.e.toString().substringAfter(": "),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    when(val response = viewModel.revokeAccessResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data == true) {
                TrackingServices.finishServices(applicationContext)
                Toast.makeText(
                    LocalContext.current,
                    "Account deleted",
                    Toast.LENGTH_SHORT
                ).show()
                LaunchedEffect(response) {
                    navToSignInScreen()
                }
            }
        }
        is Response.Failure -> LaunchedEffect(Unit) {
            Log.d("AUTH", response.toString())
        }
    }

    when(val response = viewModel.settingsResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {
            if (response.data != null) {
                viewModel.onEvent(SettingsUIEvent.SettingsChanged(response.data))
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    when(val response = viewModel.profileResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {
            if (response.data != null) {
                viewModel.onEvent(SettingsUIEvent.ProfileChanged(response.data))
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    when(val response = viewModel.profileSaveResponse) {
        is Response.Loading -> Unit
        is Response.Success -> {
            if(response.data == true) {
                Toast.makeText(
                    LocalContext.current,
                    "Profile info's saved",
                    Toast.LENGTH_SHORT
                ).show()
                viewModel.onEvent(SettingsUIEvent.ResetProfileSaveResponse)
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    val (isDialogShow, changeVisibleDialog) = rememberSaveable {
        mutableStateOf(false)
    }

    /**
     * Beginning of composable UI
     */
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(all = 10.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
             Card(
                shape = MaterialTheme.shapes.small,
                backgroundColor = ColorPalette.grey1,
                modifier = Modifier.fillMaxWidth(),
                elevation = 3.dp,
            ) {
                Column(
                    modifier = Modifier.padding(vertical = 20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    MapSettings(
                        style = viewModel.settingsUIState.collectAsState().value.style,
                        lineWeight = viewModel.settingsUIState.collectAsState().value.lineWeight,
                        lineColor = viewModel.settingsUIState.collectAsState().value.lineColor,
                        changeWeight = {
//                            settings.mapConfig.weight = it
//                            viewModel.updateSettings(settings)
                            viewModel.onEvent(SettingsUIEvent.LineWeightChanged(it))
                        },
                        changeMapType = {
//                            settings.mapConfig.style = it
//                            viewModel.updateSettings(settings)
                            viewModel.onEvent(SettingsUIEvent.StyleChanged(it))
                        },
                        changeColorMap = {
//                            settings.mapConfig.colorValue = it.hashCode()
//                            viewModel.updateSettings(settings)
                            viewModel.onEvent(SettingsUIEvent.LineColorChanged(it))
                        },
                    )
                }
            }

            Spacer(modifier = Modifier.height(10.dp))

            Card(
                shape = MaterialTheme.shapes.small,
                backgroundColor = ColorPalette.grey1,
                modifier = Modifier.fillMaxWidth(),
                elevation = 3.dp,
            ) {
                Column(
                    modifier = Modifier.padding(vertical = 20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    MetricConfig(
                        metricType = viewModel.settingsUIState.collectAsState().value.metrics,
                        changeMetric = {
                            viewModel.onEvent(SettingsUIEvent.MetricsChanged(it))
                        },
                    )
                }
            }

            Spacer(modifier = Modifier.height(10.dp))

            Card(
                shape = MaterialTheme.shapes.small,
                backgroundColor = ColorPalette.grey1,
                modifier = Modifier.fillMaxWidth(),
                elevation = 3.dp,
            ) {
                Column(
                    modifier = Modifier.padding(vertical = 20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = "Profile",
                        color = TextPalette.light,
                        textAlign = TextAlign.Center,
                        style = TextStyle(
                            fontSize = 20.sp,
                        )
                    )
                    SubmitButton(
                        onClick = {
                            viewModel.onEvent(SettingsUIEvent.ResetProfileState)
                            changeVisibleDialog(true)
                        },
                        text = "Modify profile info's"
                    )
                }
            }

            Spacer(modifier = Modifier.height(10.dp))

            Card(
                shape = MaterialTheme.shapes.small,
                backgroundColor = ColorPalette.grey1,
                modifier = Modifier.fillMaxWidth(),
                elevation = 3.dp,
            ) {
                Column(
                    modifier = Modifier.padding(vertical = 20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = "Account",
                        color = TextPalette.light,
                        textAlign = TextAlign.Center,
                        style = TextStyle(
                            fontSize = 20.sp,
                        )
                    )
                    SubmitButton(
                        onClick = { viewModel.onEvent(SettingsUIEvent.SignOut) },
                        text = "Sign out"
                    )
                    SubmitButton(
                        onClick = { viewModel.onEvent(SettingsUIEvent.RevokeGoogleAccess) },
                        text = "Revoke Google"
                    )
                }

            }
        }
    }

    if (isDialogShow) {
        EditInfoDialog(
            settingsUIState = viewModel.settingsUIState.collectAsState().value,
            changeAge = { viewModel.onEvent(SettingsUIEvent.AgeChanged(it)) },
            changeHeight = { viewModel.onEvent(SettingsUIEvent.HeightChanged(it)) },
            changeWeight = { viewModel.onEvent(SettingsUIEvent.WeightChanged(it)) },
            changeGender = { viewModel.onEvent(SettingsUIEvent.GenderChanged(it)) },
            changedFocusAge = { viewModel.onEvent(SettingsUIEvent.AgeFocusChanged) },
            changedFocusHeight = { viewModel.onEvent(SettingsUIEvent.HeightFocusChanged) },
            changedFocusWeight = { viewModel.onEvent(SettingsUIEvent.WeightFocusChanged) },
            hiddenDialog = { changeVisibleDialog(false) },
            saveProfile = { viewModel.onEvent(SettingsUIEvent.SubmitProfile) },
        )
    }

}
