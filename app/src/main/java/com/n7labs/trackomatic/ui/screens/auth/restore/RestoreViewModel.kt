package com.n7labs.trackomatic.ui.screens.auth.restore

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.n7labs.trackomatic.domain.events.RestoreUIEvent
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.SendPasswordResetResponse
import com.n7labs.trackomatic.domain.states.RestoreUIState
import com.n7labs.trackomatic.domain.usecase.auth.SendPasswordResetEmail
import com.n7labs.trackomatic.domain.usecase.validatable.ValidateEmail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RestoreViewModel @Inject constructor(
    private val sendPasswordResetEmail: SendPasswordResetEmail,
    private val validateEmail: ValidateEmail
): ViewModel() {

    var restoreResponse by mutableStateOf<SendPasswordResetResponse>(Response.Success(false))
        private set

    private val _restoreUIState = MutableStateFlow(RestoreUIState())
    val restoreUIState = _restoreUIState.asStateFlow()

    fun onEvent(event: RestoreUIEvent) = when(event) {
        is RestoreUIEvent.EmailChanged -> {
            _restoreUIState.value = restoreUIState.value.copy(email = event.email)
        }
        is RestoreUIEvent.EmailFocusChanged -> {
            val result = validateEmail(_restoreUIState.value.email)
            _restoreUIState.value = restoreUIState.value.copy(
                emailError = result.errorMessage
            )
        }
        is RestoreUIEvent.Submit -> {
            submitData()
        }
    }

    private fun submitData() {
        val emailResult = validateEmail(_restoreUIState.value.email)

        val hasError = listOf(
            emailResult
        ).any {
            !it.isSuccessful
        }

        _restoreUIState.value = restoreUIState.value.copy(
            emailError = emailResult.errorMessage
        )

        if (!hasError) {
            viewModelScope.launch {
                restoreResponse = Response.Loading
                restoreResponse = sendPasswordResetEmail(
                    _restoreUIState.value.email
                )
            }
        }
    }
}