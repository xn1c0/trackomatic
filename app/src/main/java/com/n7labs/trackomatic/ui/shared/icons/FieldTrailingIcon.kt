package com.n7labs.trackomatic.ui.shared.icons

import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.ui.theme.ColorPalette

@Composable
fun FieldTrailingIcon(
    isHidden: Boolean
) {
    if(isHidden) {
        Icon(
            painter = painterResource(id = R.drawable.ic_eye_close),
            contentDescription = "trailing_field_icon",
            tint = ColorPalette.Blue.medium,
            modifier = Modifier.size(24.dp)
        )
    }
    else {
        Icon(
            painter = painterResource(id = R.drawable.ic_eye_open),
            contentDescription = "trailing_field_icon",
            tint = ColorPalette.Blue.medium,
            modifier = Modifier.size(24.dp)
        )
    }

}