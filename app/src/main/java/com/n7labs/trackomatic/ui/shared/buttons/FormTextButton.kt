package com.n7labs.trackomatic.ui.shared.buttons

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.n7labs.trackomatic.ui.theme.Nova
import com.n7labs.trackomatic.ui.theme.TextPalette
import com.n7labs.trackomatic.ui.theme.TrackOMaticTheme

@Composable
fun FormTextButton(
    onClick: () -> Unit,
    text: String,
    modifier: Modifier = Modifier,
    color: Color = TextPalette.medium,
    fontFamily: FontFamily = Nova,
    fontSize: TextUnit = 12.sp
) {
    TextButton(
        onClick = onClick,
        contentPadding = PaddingValues(vertical = 0.dp),
        modifier = modifier
    )
    {
        Text(
            text = text,
            color = color,
            fontFamily = fontFamily,
            fontSize = fontSize,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun FormTextButtonPreview() {
    TrackOMaticTheme {
        FormTextButton({}, "SampleText")
    }
}