package com.n7labs.trackomatic.ui.navigation.bars

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Place
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector

sealed class BottomBarContent(
    val route: String,
    val title: String,
    val icon: ImageVector,
) {
    object Home : BottomBarContent(
        route = "home",
        title = "Home",
        icon = Icons.Default.Home,
    )

    object ToM : BottomBarContent(
        route = "tom",
        title = "ToM",
        icon = Icons.Default.Place
    )

    object Profile : BottomBarContent(
        route = "profile",
        title = "Profile",
        icon = Icons.Default.Person
    )

    object Settings : BottomBarContent(
        route = "settings",
        title = "Settings",
        icon = Icons.Default.Settings
    )
}