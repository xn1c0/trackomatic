package com.n7labs.trackomatic.ui.theme

import androidx.compose.ui.graphics.Color

val SystemBarColor = ColorPalette.Blue.medium

object ColorPalette {
    object Blue {
        val light = Color(0xFF8ECAE6)
        val medium = Color(0xFF219EBC)
        val dark = Color(0xFF023047)
    }
    // val yellow = Color(0xFFFFB703)
    val orange = Color(0xFFFB8500)
    val white = Color(0xFFFFFFFF)
    val grey1 = Color(0xFF282D36)
    val grey2 = Color(0xFF1E222A)
}

object TextPalette {
    val ultralight = Color(0xFFDEE0E4)
    val light = Color(0xFF9EA4AF)
    val medium = Color(0xFF2D3036)
    val dark = Color(0xFF1A1B1D)
}