package com.n7labs.trackomatic.ui.screens.main.home.components

import android.util.Log
import androidx.compose.runtime.Composable
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.Sessions
import com.n7labs.trackomatic.ui.screens.main.home.HomeViewModel
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar

@Composable
fun Sessions(
    viewModel: HomeViewModel,
    sessionContent: @Composable (sessions: Sessions) -> Unit
) {
    when(val response = viewModel.sessionsResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {
            if (response.data != null)
                sessionContent(response.data)
        }
        is Response.Failure -> Log.d("CCC", response.e.toString())
    }

    when(val response = viewModel.deleteSessionResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {}
        is Response.Failure -> Log.d("CCC", response.e.toString())
    }

    when(val response = viewModel.deleteImageResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {}
        is Response.Failure -> Log.d("CCC", response.e.toString())
    }

}