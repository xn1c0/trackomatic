package com.n7labs.trackomatic.ui.screens.main.profile.components

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.core.*
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.domain.repository.Sessions
import com.n7labs.trackomatic.ui.shared.graph.BarGraph
import com.n7labs.trackomatic.ui.shared.graph.BarType
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.TextPalette
import java.util.*

@RequiresApi(Build.VERSION_CODES.N)
@Composable
fun GraphSessions(
    sessions: Sessions,
    modifier: Modifier = Modifier,
    metricType: MetricType,
) {
    var tTime = 0L
    var tDistance = 0.0
    var tCalories = 0.0
    var aSpeed = 0.0

    sessions.forEach { session ->
        tTime += session.timeRunInMillis
        tDistance += session.distanceInMeters
        tCalories += session.caloriesBurned
        aSpeed += session.avgSpeedInMeters
    }

    aSpeed /= sessions.size
    if (aSpeed.isNaN()) aSpeed = 0.0

    Column(modifier = modifier) {
        Spacer(modifier = Modifier.height(20.dp))
        Column(modifier = modifier, verticalArrangement = Arrangement.SpaceEvenly) {
            Row(modifier = Modifier.fillMaxWidth()) {
                TextInfo(
                    titleInfo = "Total time",
                    valueInfo = tTime.toFullFormatTime(true),
                    modifier = Modifier.weight(.5f)
                )
                TextInfo(
                    titleInfo = "Total distance",
                    valueInfo = tDistance.toMeters(metricType),
                    modifier = Modifier.weight(.5f)
                )
            }
            Spacer(modifier = Modifier.height(20.dp))
            Row(modifier = Modifier.fillMaxWidth()) {
                TextInfo(titleInfo = "Calories burned",
                    valueInfo = tCalories.toCaloriesBurned(metricType),
                    modifier = Modifier.weight(.5f))
                TextInfo(titleInfo = "Average speed",
                    valueInfo = aSpeed.toAVGSpeed(metricType),
                    modifier = Modifier.weight(.5f))
            }
        }

        Spacer(modifier = Modifier.height(30.dp))
        
        Column(
            modifier = Modifier
                .padding(horizontal = 20.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            val dataList = mutableListOf<Float>()

            val floatValue = mutableListOf<Float>()
            val datesList = mutableListOf<String>() // horizontal

//            val dataList = mutableListOf(30,60,90,50,70)
//            val floatValue = mutableListOf<Float>()
//            val datesList = mutableListOf(2,3,4,5,6)

//            dataList.forEachIndexed { index, value ->
//
//                floatValue.add(index = index, element = value.toFloat()/dataList.max().toFloat())
//
//            }

            var max = 100f
            if(sessions.isNotEmpty()) {
                max = sessions.maxWith(
                    Comparator.comparingDouble { it.avgSpeedInMeters }
                ).avgSpeedInMeters.toFloat()

                sessions.forEachIndexed { index, session ->
                    val e = session.avgSpeedInMeters.toFloat() / max
                    floatValue.add(
                        index = index,
                        element = if (e.isNaN()) 0f else e
                    )
                    val date = session.timestamp.toDateFormat()
                    datesList.add(index = index, element = date)
                }
            }

            max = when (metricType) {
                MetricType.METERS -> max
                MetricType.KILOS -> ((max * 3.6).toFloat())
            }

            dataList.add(max)

            BarGraph(
                graphBarData = floatValue,
                xAxisScaleData = datesList,
                barData_ = dataList,
                height = 300.dp,
                roundType = BarType.TOP_CURVED,
                barWidth = 20.dp,
                barColor = ColorPalette.orange,
                barArrangement = Arrangement.SpaceEvenly
            )

            Text(
                text = "Average speed graph, ordered by timestamp",
                color = ColorPalette.Blue.medium,
                style = MaterialTheme.typography.caption,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis, textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
private fun TextInfo(titleInfo: String, valueInfo: String, modifier: Modifier = Modifier) {
    Column(horizontalAlignment = Alignment.CenterHorizontally, modifier = modifier) {
        Text(
            text = valueInfo,
            color = TextPalette.light,
            style = MaterialTheme.typography.h5,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = titleInfo,
            color = ColorPalette.Blue.medium,
            style = MaterialTheme.typography.caption,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis, textAlign = TextAlign.Center
        )
    }
}