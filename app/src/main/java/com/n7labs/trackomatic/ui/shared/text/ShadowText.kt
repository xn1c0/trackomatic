package com.n7labs.trackomatic.ui.shared.text

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.n7labs.trackomatic.ui.theme.Nova

@Composable
fun ShadowText(
    text: String,
    fgColor: Color = Color.White,
    bgColor: Color = Color.Black,

) {
    Text(
        text = text,
        color = fgColor,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        style = TextStyle(
            fontSize = 50.sp,
            shadow = Shadow(
                color = bgColor,
                offset = Offset(3.0f, 3.0f),
                blurRadius = 5f
            )
        ),
        textAlign = TextAlign.Center,
        fontWeight = FontWeight.Bold,
        fontFamily = Nova,
    )
}