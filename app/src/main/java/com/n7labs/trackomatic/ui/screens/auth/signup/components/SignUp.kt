package com.n7labs.trackomatic.ui.screens.auth.signup.components

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.ui.screens.auth.signup.SignUpViewModel
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar

@Composable
fun SignUp(
    viewModel: SignUpViewModel,
    navToSignInScreen: () -> Unit
) {
    when(val response = viewModel.signUpResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> {
            if(response.data == true) {
                Toast.makeText(
                    LocalContext.current,
                    "Account correctly created!",
                    Toast.LENGTH_SHORT
                ).show()
                LaunchedEffect(key1 = viewModel.signUpResponse) {
                    navToSignInScreen()
                }
            }
        }
        is Response.Failure -> {
            Toast.makeText(
                LocalContext.current,
                response.e.toString().substringAfter(": "),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}