package com.n7labs.trackomatic.ui.screens.auth.signup

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.n7labs.trackomatic.domain.events.SignUpUIEvent
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.CreateUserWithEmailAndPasswordResponse
import com.n7labs.trackomatic.domain.states.SignUpUIState
import com.n7labs.trackomatic.domain.usecase.auth.CreateUserWithEmailAndPassword
import com.n7labs.trackomatic.domain.usecase.validatable.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel  @Inject constructor(
    private val createUserWithEmailAndPassword: CreateUserWithEmailAndPassword,
    private val validateUsername: ValidateUsername,
    private val validateEmail: ValidateEmail,
    private val validatePassword: ValidatePassword,
    private val validatePasswordCheck: ValidatePasswordCheck,
    private val validateTerms: ValidateTerms
): ViewModel() {

    var signUpResponse by mutableStateOf<CreateUserWithEmailAndPasswordResponse>(Response.Success(false))
        private set

    private val _signUpUIState = MutableStateFlow(SignUpUIState())
    val signUpUIState = _signUpUIState.asStateFlow()

    fun onEvent(event: SignUpUIEvent) = when(event) {
        is SignUpUIEvent.UsernameChanged -> {
            _signUpUIState.value = signUpUIState.value.copy(username = event.username)
        }
        is SignUpUIEvent.EmailChanged -> {
            _signUpUIState.value = signUpUIState.value.copy(email = event.email)
        }
        is SignUpUIEvent.PasswordChanged -> {
            _signUpUIState.value = signUpUIState.value.copy(password = event.password)
        }
        is SignUpUIEvent.PasswordCheckChanged -> {
            _signUpUIState.value = signUpUIState.value.copy(passwordCheck = event.passwordCheck)
        }
        is SignUpUIEvent.TermsChanged -> {
            _signUpUIState.value = signUpUIState.value.copy(acceptedTerms = event.acceptedTerms)
        }
        is SignUpUIEvent.UsernameFocusChanged -> {
            val result = validateUsername(_signUpUIState.value.username)
            _signUpUIState.value = signUpUIState.value.copy(
                usernameError = result.errorMessage
            )
        }
        is SignUpUIEvent.EmailFocusChanged -> {
            val result = validateEmail(_signUpUIState.value.email)
            _signUpUIState.value = signUpUIState.value.copy(
                emailError = result.errorMessage
            )
        }
        is SignUpUIEvent.PasswordFocusChanged -> {
            val result = validatePassword(_signUpUIState.value.password)
            _signUpUIState.value = signUpUIState.value.copy(
                passwordError = result.errorMessage
            )
        }
        is SignUpUIEvent.PasswordCheckFocusChanged -> {
            val result = validatePasswordCheck(
                _signUpUIState.value.password,
                _signUpUIState.value.passwordCheck
            )
            _signUpUIState.value = signUpUIState.value.copy(
                passwordCheckError = result.errorMessage
            )
        }
        is SignUpUIEvent.SignUp -> submitSignUp()
    }

    private fun submitSignUp() {
        val usernameResult = validateUsername(_signUpUIState.value.username)
        val emailResult = validateEmail(_signUpUIState.value.email)
        val passwordResult = validatePassword(_signUpUIState.value.password)
        val passwordCheckResult = validatePasswordCheck(
            _signUpUIState.value.password,
            _signUpUIState.value.passwordCheck
        )
        val acceptedTermsResult = validateTerms(_signUpUIState.value.acceptedTerms)

        val hasError = listOf(
            usernameResult,
            emailResult,
            passwordResult,
            passwordCheckResult,
            acceptedTermsResult
        ).any {
            !it.isSuccessful
        }

        _signUpUIState.value = signUpUIState.value.copy(
            usernameError = usernameResult.errorMessage,
            emailError = emailResult.errorMessage,
            passwordError = passwordResult.errorMessage,
            passwordCheckError = passwordCheckResult.errorMessage,
            acceptedTermsError = acceptedTermsResult.errorMessage,
        )

        if(!hasError) {
            viewModelScope.launch {
                signUpResponse = Response.Loading
                signUpResponse = createUserWithEmailAndPassword(
                    _signUpUIState.value.username,
                    _signUpUIState.value.email,
                    _signUpUIState.value.password,
                )
            }
        }
    }

}