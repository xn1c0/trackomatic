package com.n7labs.trackomatic.ui.screens.main.home.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.domain.model.Session
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.domain.repository.Sessions
import com.n7labs.trackomatic.ui.other.EmptyScreen

@Composable
fun SessionsContent(
    sessions: Sessions,
    metricType: MetricType,
    toggleDialog: (s: Session) -> Unit
) {
    if(sessions.isEmpty()) {
        EmptyScreen(
            animation = R.raw.empty,
            textEmpty = "A little bit empty here isn't it?",
            modifier = Modifier.padding(20.dp)
        )
    } else {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 10.dp)
        ) {
            items(
                items = sessions
            ) { session ->
                SessionCard(
                    session = session,
                    metricType = metricType,
                    showDialogStatistics = { s ->
                        toggleDialog(s)
                    }
                )
                if (session != sessions.last())
                    Spacer(modifier = Modifier.height(10.dp))
            }
        }   
    }
}