package com.n7labs.trackomatic.ui.screens.auth.signin.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import com.n7labs.trackomatic.ui.screens.auth.signin.SignInViewModel

@Composable
fun IsAuthenticated(
    viewModel: SignInViewModel,
    navToVerifyScreen: () -> Unit,
) {
    LaunchedEffect(Unit) {
        if (viewModel.isAuthenticated) {
            navToVerifyScreen()
        }
    }
}