package com.n7labs.trackomatic.ui.shared.icons

import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.ui.theme.ColorPalette

@Composable
fun FieldLeadingIcon(
    painter: Painter,
    hasError: Boolean,
) {
    Icon(
        painter = painter,
        contentDescription = "leading_field_icon",
        tint = if (hasError) ColorPalette.orange else ColorPalette.Blue.medium,
        modifier = Modifier.size(24.dp)
    )
}