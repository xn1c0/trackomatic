package com.n7labs.trackomatic.ui.screens.main.tom

import android.graphics.Bitmap
import android.net.Uri
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asAndroidBitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import com.google.maps.android.SphericalUtil
import com.n7labs.trackomatic.domain.model.*
import com.n7labs.trackomatic.domain.repository.*
import com.n7labs.trackomatic.domain.usecase.images.InsertImage
import com.n7labs.trackomatic.domain.usecase.profile.GetProfile
import com.n7labs.trackomatic.domain.usecase.session.InsertSession
import com.n7labs.trackomatic.domain.usecase.setting.GetSettings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject


@HiltViewModel
class ToMViewModel @Inject constructor(
    locationRepository: TrackingRepository,
    private val getProfile: GetProfile,
    private val getSettings: GetSettings,
    private val insertSession: InsertSession,
    private val insertImage: InsertImage
): ViewModel() {

    /**
     * Responses
     */
    var insertSessionResponse by mutableStateOf<InsertSessionResponse>(Response.Success(false))
        private set
    var insertImageResponse by mutableStateOf<ImageAddResponse>(Response.Success(null))
        private set

    /**
     * Loading
     */
    private var profilesResponse by mutableStateOf<ProfileResponse>(Response.Loading)

    private val profile: UserProfile?
        get() = when(val response = profilesResponse) {
            is Response.Success -> response.data
            else -> null
        }

    var settingsResponse by mutableStateOf<SettingsResponse>(Response.Loading)
        private set

    init {
        loadProfile()
        loadSetting()
    }

    // Keeps updating data on change see profile repo
    private fun loadProfile() = viewModelScope.launch {
        getProfile().collect { response ->
            profilesResponse = response
        }
    }

    // Keeps updating data on change see profile repo
    private fun loadSetting() = viewModelScope.launch {
        getSettings().collect { response ->
            settingsResponse = response
        }
    }

    /**
     * Tracking Flow
     */
    private var isEnableAnimation by mutableStateOf(true)

    val stateTracking = locationRepository.stateTracking.flowOn(Dispatchers.IO).stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000),
        initialValue = TrackingState.WAITING
    )

    val polyData = locationRepository.lastLocationSaved.flowOn(Dispatchers.IO).stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000),
        initialValue = null
    )

     val lastLocation = locationRepository.lastLocation.filter { isEnableAnimation }.flowOn(Dispatchers.IO).stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000),
        initialValue = null
    )

    val timeRun = locationRepository.timeTracking.flowOn(Dispatchers.IO).stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000),
        initialValue = 0L
    )
    fun changeAnimation(isEnable: Boolean) {
        isEnableAnimation = isEnable
    }

    fun submitImage(bitmap: ImageBitmap) = viewModelScope.launch {
        insertImageResponse = Response.Loading

        val baos = ByteArrayOutputStream()
        bitmap.asAndroidBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data: ByteArray = baos.toByteArray()

        insertImageResponse = insertImage(data)
    }

    fun genSession(
        settings: Settings = Settings(),
        uri: Uri? = null
    ): Session {
        val weight = profile!!.weight
        val currentTime = System.currentTimeMillis()

        val listPolylineEncode = polyData.value!!.flatten()

        val distanceInMeters = SphericalUtil.computeLength(listPolylineEncode)
        val avgSpeedInMS = distanceInMeters / (timeRun.value / 1000f)
        val caloriesBurned = distanceInMeters * (weight / 1000f)

        val geoPoints: MutableList<GeoPoint> = mutableListOf()
        listPolylineEncode.forEach {
            geoPoints.add(GeoPoint(it.latitude, it.longitude))
        }

        return Session(
            avgSpeedInMeters = if (avgSpeedInMS.isNaN()) 0.0 else avgSpeedInMS,
            distanceInMeters = if (distanceInMeters.isNaN()) 0.0 else distanceInMeters,
            timeRunInMillis = timeRun.value,
            caloriesBurned = if (caloriesBurned.isNaN()) 0.0 else caloriesBurned,
            listPolyLine = geoPoints,
            mapConfig = MapConfig(
                settings.mapConfig.style,
                settings.mapConfig.weight,
                settings.mapConfig.colorValue
            ),
            uri = uri.toString(),
            timestamp = Timestamp(Date(currentTime)),
        )
    }
    fun submitSession(
        settings: Settings,
        uri: Uri
    ) = viewModelScope.launch {
        insertSessionResponse = Response.Loading
        insertImageResponse = Response.Loading
        insertSessionResponse = insertSession(genSession(settings, uri))
    }
}