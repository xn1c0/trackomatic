package com.n7labs.trackomatic.ui.screens.auth.signup

import android.os.Build
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.ImageLoader
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberAsyncImagePainter
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.request.ImageRequest
import coil.size.Size
import com.n7labs.trackomatic.R
import com.n7labs.trackomatic.domain.events.SignUpUIEvent
import com.n7labs.trackomatic.ui.screens.auth.signup.components.SignUp
import com.n7labs.trackomatic.ui.shared.buttons.FormCheckBox
import com.n7labs.trackomatic.ui.shared.buttons.FormTextButton
import com.n7labs.trackomatic.ui.shared.buttons.SubmitButton
import com.n7labs.trackomatic.ui.shared.fields.PlainField
import com.n7labs.trackomatic.ui.shared.fields.SecretField
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.LoginBottomBoxShape
import com.n7labs.trackomatic.ui.theme.Nova
import com.n7labs.trackomatic.ui.theme.TrackOMaticTheme

@Composable
@ExperimentalCoilApi
fun SignUpScreen(
    viewModel: SignUpViewModel,
    navToSignInScreen: () -> Unit
) {

    /**
     * Used to handle recomposition based on responses
     */
    SignUp(viewModel = viewModel, navToSignInScreen = navToSignInScreen)

    /**
     * Beginning of composable UI
     */
    val imageLoader = ImageLoader.Builder(LocalContext.current)
        .components {
            if (Build.VERSION.SDK_INT >= 28) {
                add(ImageDecoderDecoder.Factory())
            } else {
                add(GifDecoder.Factory())
            }
        }
        .build()

    val localFocus = LocalFocusManager.current

    TrackOMaticTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = ColorPalette.Blue.medium
        ) {
            Box(
                contentAlignment = Alignment.TopCenter
            ) {
                Image(
                    modifier = Modifier.fillMaxWidth(0.7f),
                    contentScale = ContentScale.FillWidth,
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(LocalContext.current)
                            .data(data = R.drawable.signup_illustration_light)
                            .apply(block = fun ImageRequest.Builder.() {
                                size(Size.ORIGINAL)
                            }).build(), imageLoader = imageLoader
                    ),
                    contentDescription = "signup_illustration"
                )
            }
            Box (
                contentAlignment = Alignment.BottomCenter
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "Create your account",
                        color = Color.White,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                        style = TextStyle(
                            fontSize = 30.sp,
                            shadow = Shadow(
                                color = Color.Black,
                                offset = Offset(3.0f, 3.0f),
                                blurRadius = 5f
                            )
                        ),
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight.Bold,
                        fontFamily = Nova,
                    )
                    Card(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 10.dp),
                        backgroundColor = Color.White,
                        elevation = 0.dp,
                        shape = LoginBottomBoxShape.medium
                    ) {
                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            PlainField(
                                label = "Username",
                                painter = painterResource(id = R.drawable.ic_user),
                                onTextChanged = {
                                    viewModel.onEvent(SignUpUIEvent.UsernameChanged(it))
                                },
                                keyboardType = KeyboardType.Text,
                                errorMessage = viewModel.signUpUIState.collectAsState().value.usernameError,
                                imeAction = ImeAction.Next,
                                onNext = {
                                    viewModel.onEvent(SignUpUIEvent.UsernameFocusChanged)
                                    localFocus.moveFocus(FocusDirection.Down)
                                }
                            ) {
                                // localFocus.clearFocus()
                            }
                            PlainField(
                                label = "Email",
                                painter = painterResource(id = R.drawable.ic_email),
                                onTextChanged = {
                                    viewModel.onEvent(SignUpUIEvent.EmailChanged(it))
                                },
                                keyboardType = KeyboardType.Text,
                                errorMessage = viewModel.signUpUIState.collectAsState().value.emailError,
                                imeAction = ImeAction.Next,
                                onNext = {
                                    viewModel.onEvent(SignUpUIEvent.EmailFocusChanged)
                                    localFocus.moveFocus(FocusDirection.Down)
                                }
                            ) {
                                // localFocus.clearFocus()
                            }
                            SecretField(
                                label = "Password",
                                painter = painterResource(id = R.drawable.ic_lock),
                                onTextChanged = {
                                    viewModel.onEvent(SignUpUIEvent.PasswordChanged(it))
                                },
                                keyboardType = KeyboardType.Password,
                                errorMessage = viewModel.signUpUIState.collectAsState().value.passwordError,
                                imeAction = ImeAction.Next,
                                onNext = {
                                    viewModel.onEvent(SignUpUIEvent.PasswordFocusChanged)
                                    localFocus.moveFocus(FocusDirection.Down)
                                },
                                onDone = {
                                    // localFocus.clearFocus()
                                }
                            )
                            SecretField(
                                label = "Password Check",
                                painter = painterResource(id = R.drawable.ic_lock),
                                onTextChanged = {
                                    viewModel.onEvent(SignUpUIEvent.PasswordCheckChanged(it))
                                },
                                keyboardType = KeyboardType.Password,
                                errorMessage = viewModel.signUpUIState.collectAsState().value.passwordCheckError,
                                imeAction = ImeAction.Done,
                                onNext = {},
                                onDone = {
                                    viewModel.onEvent(SignUpUIEvent.PasswordCheckFocusChanged)
                                    localFocus.clearFocus()
                                }
                            )

                            FormCheckBox(
                                onCheckedChanged = {
                                    viewModel.onEvent(SignUpUIEvent.TermsChanged(it))
                                },
                                errorMessage = viewModel.signUpUIState.collectAsState().value.acceptedTermsError
                            )

                            SubmitButton(
                                onClick = { viewModel.onEvent(SignUpUIEvent.SignUp) },
                                text = "Sign Up"
                            )
                            FormTextButton(
                                onClick = { navToSignInScreen() },
                                text = "Already have a profile? Sign In!",
                            )
                        }
                    }
                }
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun SignUpScreenPreview() {
//    TrackOMaticTheme {
//        SignUpScreen()
//    }
//}