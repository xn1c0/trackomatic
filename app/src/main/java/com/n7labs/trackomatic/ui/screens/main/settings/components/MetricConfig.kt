package com.n7labs.trackomatic.ui.screens.main.settings.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.ui.shared.fields.SelectOptionConfig
import com.n7labs.trackomatic.ui.theme.TextPalette


@Composable
fun MetricConfig(
    metricType: MetricType,
    changeMetric: (MetricType) -> Unit
) {
    val listMetrics = remember { MetricType.values().toList() }
    Text(
        modifier = Modifier.fillMaxWidth(),
        text = "Metric",
        color = TextPalette.light,
        textAlign = TextAlign.Center,
        style = TextStyle(
            fontSize = 20.sp,
        )
    )
    Spacer(modifier = Modifier.height(20.dp))
    SelectOptionConfig(
        textField = "Metric Options",
        selected = metricType.name,
        listItems = listMetrics,
        listNamed = listMetrics.map { it.name },
        onChange = changeMetric
    )
}