package com.n7labs.trackomatic.ui.screens.auth.signin

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.firebase.auth.AuthCredential
import com.n7labs.trackomatic.domain.events.SignInUIEvent
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.OneTapSignInResponse
import com.n7labs.trackomatic.domain.repository.SignInWithEmailAndPasswordResponse
import com.n7labs.trackomatic.domain.repository.SignInWithGoogleResponse
import com.n7labs.trackomatic.domain.states.SignInUIState
import com.n7labs.trackomatic.domain.usecase.auth.IsUserAuthenticated
import com.n7labs.trackomatic.domain.usecase.auth.OneTapSignInWithGoogle
import com.n7labs.trackomatic.domain.usecase.auth.SignInWithEmailAndPassword
import com.n7labs.trackomatic.domain.usecase.auth.SignInWithGoogle
import com.n7labs.trackomatic.domain.usecase.validatable.ValidateEmail
import com.n7labs.trackomatic.domain.usecase.validatable.ValidationResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val isUserAuthenticated: IsUserAuthenticated,
    private val oneTapSignInWithGoogle: OneTapSignInWithGoogle,
    private val signInWithGoogle: SignInWithGoogle,
    private val signInWithEmailAndPassword: SignInWithEmailAndPassword,
    private val validateEmail: ValidateEmail,
    val oneTapClient: SignInClient
): ViewModel() {

    val isAuthenticated get() = isUserAuthenticated()

    var oneTapSignInResponse by mutableStateOf<OneTapSignInResponse>(Response.Success(null))
        private set
    var signInWithGoogleResponse by mutableStateOf<SignInWithGoogleResponse>(Response.Success(false))
        private set
    var signInResponse by mutableStateOf<SignInWithEmailAndPasswordResponse>(Response.Success(false))
        private set

    private val _signInUIState = MutableStateFlow(SignInUIState())
    val signInUIState get() = _signInUIState.asStateFlow()

    fun onEvent(event: SignInUIEvent) = when(event) {
        is SignInUIEvent.EmailChanged -> {
            _signInUIState.value = signInUIState.value.copy(email = event.email)
        }
        is SignInUIEvent.PasswordChanged -> {
            _signInUIState.value = signInUIState.value.copy(password = event.password)
        }
        is SignInUIEvent.EmailFocusChanged -> {
            val result = validateEmail(email = _signInUIState.value.email)
            _signInUIState.value = signInUIState.value.copy(
                emailError = result.errorMessage
            )
        }
        is SignInUIEvent.PasswordFocusChanged -> {
            val result = isEmpty(_signInUIState.value.password)
            _signInUIState.value = signInUIState.value.copy(
                passwordError = result.errorMessage
            )
        }
        is SignInUIEvent.BeginGoogleSignIn -> {
            beginGoogleSignIn()
        }
        is SignInUIEvent.CompleteGoogleSignIn -> {
            completeGoogleSignIn(event.googleCredential)
        }
        is SignInUIEvent.EmailSignIn -> {
            submitEmailSignIn()
        }
    }

    private fun beginGoogleSignIn() {
        viewModelScope.launch {
            oneTapSignInResponse = Response.Loading
            oneTapSignInResponse = oneTapSignInWithGoogle()
        }
    }

    private fun completeGoogleSignIn(googleCredential: AuthCredential) {
        viewModelScope.launch {
            oneTapSignInResponse = Response.Loading
            signInWithGoogleResponse = signInWithGoogle(googleCredential)
        }
    }

    private fun submitEmailSignIn() {
        val emailResult = validateEmail(email = _signInUIState.value.email)
        val passwordResult = isEmpty(_signInUIState.value.password)

        val hasError = listOf(
            emailResult,
            passwordResult
        ).any {
            !it.isSuccessful
        }

        _signInUIState.value = signInUIState.value.copy(
            emailError = emailResult.errorMessage,
            passwordError = passwordResult.errorMessage
        )

        if (!hasError) {
            viewModelScope.launch {
                signInResponse = Response.Loading
                signInResponse = signInWithEmailAndPassword(
                    _signInUIState.value.email,
                    _signInUIState.value.password
                )
            }
        }
    }

    private fun isEmpty(target: String): ValidationResult {
        return if(target.isEmpty())
            ValidationResult.Invalid.EmptyError
        else
            ValidationResult.Valid
    }
}