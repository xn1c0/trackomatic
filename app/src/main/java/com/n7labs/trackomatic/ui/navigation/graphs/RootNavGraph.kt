package com.n7labs.trackomatic.ui.navigation.graphs

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.n7labs.trackomatic.ui.navigation.bars.BottomBarScreen
import com.n7labs.trackomatic.ui.navigation.bars.TopBarScreen
import com.n7labs.trackomatic.ui.theme.ColorPalette

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun NavigationGraph(
    navController: NavHostController
) {
    Scaffold(
        topBar = { TopBarScreen(navController) },
        bottomBar = { BottomBarScreen(navController) },
        backgroundColor = ColorPalette.Blue.medium
    ) { innerPadding ->
        Box(modifier = Modifier.padding(innerPadding).background(ColorPalette.grey2)) {
            NavHost(
                navController = navController,
                route = Graph.ROOT,
                startDestination = Graph.LAUNCH
            ) {
                launchNavGraph(navController = navController)
                authNavGraph(navController = navController)
                mainNavGraph(navController = navController)
            }
        }
    }

}

object Graph {
    const val ROOT = "root"
    const val LAUNCH = "launch"
    const val AUTHENTICATION = "auth"
    const val MAIN = "main"
}