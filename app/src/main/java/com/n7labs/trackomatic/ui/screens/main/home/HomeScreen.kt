package com.n7labs.trackomatic.ui.screens.main.home

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Session
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.ui.screens.main.home.components.Sessions
import com.n7labs.trackomatic.ui.screens.main.home.components.SessionsContent
import com.n7labs.trackomatic.ui.shared.bars.ProgressBar
import com.n7labs.trackomatic.ui.shared.dialogs.DialogStatistics

@Composable
@ExperimentalCoilApi
fun HomeScreen(
    viewModel: HomeViewModel,
) {

    val settings by remember { mutableStateOf(Settings()) }

    when(val response = viewModel.settingsResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success ->  {
            if (response.data != null) {
                settings.mapConfig = response.data.mapConfig
                settings.metrics = response.data.metrics
                settings.userID = response.data.userID
            }
        }
        is Response.Failure -> Log.d("Male", response.e.toString())
    }

    val (isDialogShow, changeVisibleDialog) = rememberSaveable {
        mutableStateOf(false)
    }

    val (dialogSession, dialogSessionSetter) = rememberSaveable {
        mutableStateOf(Session())
    }

    Log.d("SSS", "HOME2")

    Column(
        modifier = Modifier.padding(horizontal = 10.dp).padding(top = 10.dp)
    ) {
        Sessions(
            viewModel = viewModel,
            sessionContent = { sessions ->
                SessionsContent(
                    sessions = sessions,
                    metricType = settings.metrics,
                    toggleDialog = { s ->
                        dialogSessionSetter(
                            dialogSession.copy(
                                avgSpeedInMeters = s.avgSpeedInMeters,
                                distanceInMeters = s.distanceInMeters,
                                timeRunInMillis = s.timeRunInMillis,
                                caloriesBurned = s.caloriesBurned,
                                listPolyLine = s.listPolyLine,
                                timestamp = s.timestamp,
                                mapConfig = s.mapConfig,
                                uri = s.uri,
                                sessionID = s.sessionID,
                                userID = s.userID
                            )
                        )
                        changeVisibleDialog(true)
                    }
                )
            }
        )
    }

    if (isDialogShow)
        DialogStatistics(
            hiddenDialog = { changeVisibleDialog(false) },
            deleteSession = { id, uri ->
                viewModel.dImage(uri)
                viewModel.dSession(id)
            },
            session = dialogSession,
            metricType = settings.metrics,
        )

}