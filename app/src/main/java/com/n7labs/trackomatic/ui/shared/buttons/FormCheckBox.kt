package com.n7labs.trackomatic.ui.shared.buttons

import androidx.compose.foundation.layout.*
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxColors
import androidx.compose.material.CheckboxDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.n7labs.trackomatic.ui.shared.text.FormText
import com.n7labs.trackomatic.ui.shared.text.FormTextErrorMessage
import com.n7labs.trackomatic.ui.theme.ColorPalette
import com.n7labs.trackomatic.ui.theme.Shapes
import com.n7labs.trackomatic.ui.theme.TextPalette

@Composable
fun FormCheckBox(
    errorMessage: String? = null,
    onCheckedChanged: ((Boolean) -> Unit)? = null,
    colors: CheckboxColors = CheckboxDefaults.colors(
        checkedColor = ColorPalette.Blue.medium,
        uncheckedColor = TextPalette.light
    )
) {
    var value by remember { mutableStateOf(false) }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp)
            .padding(horizontal = 25.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Checkbox(
            modifier = Modifier.clip(shape = Shapes.medium),
            checked = value,
            onCheckedChange = {
                value = it
                if (onCheckedChanged != null) {
                    onCheckedChanged(it)
                }
            },
            colors = colors,

        )
        Spacer(modifier = Modifier.width(6.dp))
        Column {
            FormText(text = "Accept terms and conditions")
            FormTextErrorMessage(msg = errorMessage)
        }
    }

}

@Preview
@Composable
fun FormCheckBoxPreview() {
    FormCheckBox()
}