package com.n7labs.trackomatic.data.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.repository.ImageAddResponse
import com.n7labs.trackomatic.domain.repository.ImageRemoveResponse
import com.n7labs.trackomatic.domain.repository.ImageRepository
import kotlinx.coroutines.tasks.await
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MyImageRepository @Inject constructor(
    private val auth: FirebaseAuth,
    private val storage: FirebaseStorage,
) : ImageRepository {
    override suspend fun addImageToFirebaseStorage(data: ByteArray): ImageAddResponse {
        return try {
            val downloadUrl = storage.reference.child("${auth.currentUser!!.uid}/${UUID.randomUUID()}")
                .putBytes(data).await()
                .storage.downloadUrl.await()
            Response.Success(downloadUrl)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override suspend fun removeImageFromFirebaseStorage(uri: String): ImageRemoveResponse {
        return try {
            storage.getReferenceFromUrl(uri).delete().await()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }
}