package com.n7labs.trackomatic.data.repository

import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FieldValue.serverTimestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.n7labs.trackomatic.core.Constants
import com.n7labs.trackomatic.core.Constants.CREATED_AT_FIELD
import com.n7labs.trackomatic.core.Constants.DISPLAY_NAME_FIELD
import com.n7labs.trackomatic.core.Constants.EMAIL_FIELD
import com.n7labs.trackomatic.core.Constants.PHOTO_URL_FIELD
import com.n7labs.trackomatic.core.Constants.SIGN_IN_REQUEST
import com.n7labs.trackomatic.core.Constants.SIGN_UP_REQUEST
import com.n7labs.trackomatic.domain.adapters.FirestoreAdapter
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.domain.model.UserProfile
import com.n7labs.trackomatic.domain.repository.*
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Note to self
 * the .await is used to wait the competition of a task without blocking the thread
 * it is used because the This suspending function is cancellable.
 * If the Job of the current coroutine is cancelled or completed while this suspending
 * function is waiting, this function stops waiting for the completion stage and
 * immediately resumes with CancellationException.
 */

@Singleton
class MyAuthRepository @Inject constructor(
    private val auth: FirebaseAuth,
    private var oneTapClient: SignInClient,
    private var signInClient: GoogleSignInClient,
    @Named(SIGN_IN_REQUEST)
    private var signInRequest: BeginSignInRequest,
    @Named(SIGN_UP_REQUEST)
    private var signUpRequest: BeginSignInRequest,
    private val firestore: FirebaseFirestore,
    private val adapter: FirestoreAdapter,
): AuthRepository {

    override val isUserAuthenticatedInFireBase: Boolean
        get() = auth.currentUser != null

    override val isUserEmailVerifiedInFireBase: Boolean
        get() = auth.currentUser?.isEmailVerified == true


    override suspend fun fireOneTapSignInWithGoogle(): OneTapSignInResponse {
        return try {
            // send a first request "signin" with oneTapClient
            val signInResult = oneTapClient.beginSignIn(signInRequest).await()
            Response.Success(signInResult)
        } catch (e: Exception) {
            try {
                // first request failed. now make a new one "signup"
                val signUpResult = oneTapClient.beginSignIn(signUpRequest).await()
                Response.Success(signUpResult)
            } catch (e: Exception) {
                Response.Failure(e)
            }
        }
    }

    override suspend fun fireSignInWithGoogle(googleCredential: AuthCredential): SignInWithGoogleResponse {
        return try {
            // request to sign in to firebaseAuth with googleCredentials (a tuple of email...)
            val authResult = auth.signInWithCredential(googleCredential).await()
            // if it is a new user, add it to firestore db
            val isNewUser = authResult.additionalUserInfo?.isNewUser ?: false
            if (isNewUser) {
                addUserToFirestore()
                setupRecords()
            }
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override suspend fun fireSignInWithEmailAndPassword(
        email: String,
        password: String
    ): SignInWithEmailAndPasswordResponse {
        return try {
            // request to sign in with email and password to firebaseAuth
            auth.signInWithEmailAndPassword(email, password).await()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override suspend fun fireCreateUserWithEmailAndPassword(
        username:String,
        email: String,
        password: String
    ): CreateUserWithEmailAndPasswordResponse {
        return try {
            // request the creation of a new user
            val result = auth.createUserWithEmailAndPassword(email, password).await()
            result?.user?.updateProfile(
                UserProfileChangeRequest.Builder().setDisplayName(username).build()
            )?.await()
            setupRecords()
            auth.currentUser?.sendEmailVerification()?.await()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override suspend fun fireSendPasswordResetEmail(email: String): SendPasswordResetResponse {
        return try {
            auth.sendPasswordResetEmail(email)
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    private suspend fun addUserToFirestore() {
        auth.currentUser?.apply {
            // add the new user to the predefined firestore collection "users"
            val user = toUser()
            firestore.collection("users").document(uid).set(user).await()
        }
    }

    /**
     * Returns a Map<String, Any?>
     * This is basically the structure of the document in where
     * firebase auth stores
     */
    private fun FirebaseUser.toUser() = mapOf(
        DISPLAY_NAME_FIELD to displayName,
        EMAIL_FIELD to email,
        PHOTO_URL_FIELD to photoUrl?.toString(),
        CREATED_AT_FIELD to serverTimestamp()
    )

    private suspend fun setupRecords() {
        val settingsRef = firestore.collection(Constants.SETTINGS_COLLECTION)
        val settingID = settingsRef.document().id
        val encodedData1 = adapter(Settings(), auth.currentUser!!.uid)
        settingsRef.document(settingID).set(encodedData1).await()
        val profileRef = firestore.collection(Constants.PROFILES_COLLECTION)
        val profileID = profileRef.document().id
        val encodedData2 = adapter(UserProfile(), auth.currentUser!!.uid)
        profileRef.document(profileID).set(encodedData2).await()
    }

    override suspend fun fireSignOut(): SignOutResponse {
        return try {
            oneTapClient.signOut().await()
            auth.signOut()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override suspend fun fireRevokeAccess(): RevokeAccessResponse {
        return try {
            auth.currentUser?.apply {
                firestore.collection(Constants.USERS_COLLECTION).document(uid).delete().await()
                signInClient.revokeAccess().await()
                oneTapClient.signOut().await()
                delete().await()
            }
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    /**
     * Note to self
     * Ricordati di cambiare le regole di firestore db altrimenti da permission denied!
     */
    override suspend fun fireDeleteAccount(): DeleteAccountResponse {
        return try {
            auth.currentUser?.delete()?.await()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override suspend fun fireSendEmailVerification(): SendEmailVerificationResponse {
        return try {
            auth.currentUser?.sendEmailVerification()?.await()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

}