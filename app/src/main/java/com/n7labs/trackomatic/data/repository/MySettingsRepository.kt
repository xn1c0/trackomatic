package com.n7labs.trackomatic.data.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.n7labs.trackomatic.core.Constants
import com.n7labs.trackomatic.domain.adapters.FirestoreAdapter
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.domain.repository.SettingsRepository
import com.n7labs.trackomatic.domain.repository.SettingsSaveResponse
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MySettingsRepository @Inject constructor(
    private val auth: FirebaseAuth,
    private val firestore: FirebaseFirestore,
    private val adapter: FirestoreAdapter
): SettingsRepository {

    private val settingsRef = firestore.collection(Constants.SETTINGS_COLLECTION)

    private val userID: String
        get() = auth.currentUser!!.uid

    override suspend fun saveSettings(
        setting: Settings
    ): SettingsSaveResponse {
        return try {
            val settingID = settingsRef.whereEqualTo(
                Constants.USERID_FIELD,
                userID
            ).get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val encodedData = adapter(setting, userID)
                    settingsRef.document(task.result.documents[0].id).set(encodedData, SetOptions.merge())
                }
            }
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override fun getSettings() = callbackFlow {
        val snapshotListener = settingsRef.whereEqualTo(
            Constants.USERID_FIELD,
            auth.currentUser?.uid
        ).addSnapshotListener { snapshot, e ->
            val sessionResponse = if (snapshot != null) {
                val profile = snapshot.documents[0].toObject(Settings::class.java)
                Response.Success(profile)
            } else {
                Response.Failure(e!!)
            }
            trySend(sessionResponse)
        }
        awaitClose {
            snapshotListener.remove()
        }
    }
}