package com.n7labs.trackomatic.data.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.n7labs.trackomatic.core.Constants
import com.n7labs.trackomatic.domain.adapters.FirestoreAdapter
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.UserProfile
import com.n7labs.trackomatic.domain.repository.ProfileRepository
import com.n7labs.trackomatic.domain.repository.ProfileSaveResponse
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

/**
 * Note to self
 * the .await is used to wait the competition of a task without blocking the thread
 * it is used because the This suspending function is cancellable.
 * If the Job of the current coroutine is cancelled or completed while this suspending
 * function is waiting, this function stops waiting for the completion stage and
 * immediately resumes with CancellationException.
 */

class MyProfileRepository @Inject constructor(
    private val auth: FirebaseAuth,
    firestore: FirebaseFirestore,
    private val adapter: FirestoreAdapter
): ProfileRepository {

    private val profileRef = firestore.collection(Constants.PROFILES_COLLECTION)

    private val userID: String
        get() = auth.currentUser!!.uid

    override val displayName: String
        get() = auth.currentUser!!.displayName.toString()

    override val photoUrl: String
        get() = auth.currentUser!!.photoUrl.toString()

    override suspend fun saveProfile(
        profile: UserProfile
    ): ProfileSaveResponse {
        return try {
            profileRef.whereEqualTo(
                Constants.USERID_FIELD,
                userID
            ).get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val encodedData = adapter(profile, userID)
                    profileRef.document(task.result.documents[0].id).set(encodedData, SetOptions.merge())
                }
            }
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override fun getProfiles() = callbackFlow {
        val snapshotListener = profileRef.whereEqualTo(
            Constants.USERID_FIELD,
            auth.currentUser?.uid
        ).addSnapshotListener { snapshot, e ->
            val profileResponse = if (snapshot != null) {
                val profile = snapshot.documents[0].toObject(UserProfile::class.java)
                Response.Success(profile)
            } else {
                Response.Failure(e!!)
            }
            trySend(profileResponse)
        }
        awaitClose {
            snapshotListener.remove()
        }
    }

}