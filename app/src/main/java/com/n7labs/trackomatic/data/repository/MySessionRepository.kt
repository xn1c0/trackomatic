package com.n7labs.trackomatic.data.repository

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.n7labs.trackomatic.core.Constants
import com.n7labs.trackomatic.domain.adapters.FirestoreAdapter
import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Session
import com.n7labs.trackomatic.domain.repository.DeleteSessionResponse
import com.n7labs.trackomatic.domain.repository.InsertSessionResponse
import com.n7labs.trackomatic.domain.repository.SessionRepository
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MySessionRepository @Inject constructor(
    private val auth: FirebaseAuth,
    firestore: FirebaseFirestore,
    private val adapter: FirestoreAdapter
): SessionRepository {

    private val sessionsRef = firestore.collection(Constants.SESSION_COLLECTION)

    private val userID: String
        get() = auth.currentUser!!.uid

    override suspend fun insertSession(
        session: Session
    ): InsertSessionResponse {
        return try {
            val sessionID = sessionsRef.document().id
            val encodedData = adapter(session, sessionID, userID)
            sessionsRef.document(sessionID).set(encodedData).await()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override suspend fun deleteSession(
        sessionID: String
    ): DeleteSessionResponse {
        return try {
            Log.d("SSS", "REPO")
            sessionsRef.document(sessionID).delete().await()
            Response.Success(true)
        } catch (e: Exception) {
            Response.Failure(e)
        }
    }

    override fun getListSessionsByDate() = callbackFlow {
        val snapshotListener = sessionsRef.whereEqualTo(
            Constants.USERID_FIELD,
            auth.currentUser!!.uid
        ).orderBy(
            "timestamp",
            Query.Direction.DESCENDING
        ).addSnapshotListener { snapshot, e ->
            val sessionResponse = if (snapshot != null) {
                val sessions = snapshot.toObjects(Session::class.java)
                Response.Success(sessions)
            } else {
                Log.d("Male", e.toString())
                Response.Failure(e!!)
            }
            trySend(sessionResponse)
        }
        awaitClose {
            snapshotListener.remove()
        }
    }
}