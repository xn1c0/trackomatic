package com.n7labs.trackomatic.domain.states

data class SignInUIState(
    val email: String = "",
    val password: String = "",
    val emailError: String? = null,
    val passwordError: String? = null,
)