package com.n7labs.trackomatic.domain.states

data class ToMUIState(
    val username: String = "",
)