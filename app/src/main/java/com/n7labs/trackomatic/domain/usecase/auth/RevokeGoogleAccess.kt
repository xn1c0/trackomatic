package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.data.repository.MyAuthRepository
import com.n7labs.trackomatic.domain.repository.RevokeAccessResponse
import javax.inject.Inject

class RevokeGoogleAccess @Inject constructor(
    private val authRepository: MyAuthRepository
) {
    suspend operator fun invoke(): RevokeAccessResponse = authRepository.fireRevokeAccess()
}