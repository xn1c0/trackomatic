package com.n7labs.trackomatic.domain.repository

import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Settings
import kotlinx.coroutines.flow.Flow

typealias SettingsSaveResponse = Response<Boolean>
typealias SettingsResponse = Response<Settings>

interface SettingsRepository {

    suspend fun saveSettings(
        settings: Settings
    ): SettingsSaveResponse

    fun getSettings(): Flow<SettingsResponse>
}