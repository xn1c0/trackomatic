package com.n7labs.trackomatic.domain.model

enum class TrackingState {
    WAITING,TRACKING,PAUSE
}