package com.n7labs.trackomatic.domain.model

/**
 * Used to provide a simple mechanism to store data provided
 * by API calls and coordinate the ui to display things the right way
 */

sealed class Response<out T> {

    object Loading: Response<Nothing>()

    data class Success<out T>(
        val data: T?
    ): Response<T>()

    data class Failure(
        val e: Exception
    ): Response<Nothing>()

}