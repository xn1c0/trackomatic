package com.n7labs.trackomatic.domain.usecase.session

import com.n7labs.trackomatic.data.repository.MySessionRepository
import com.n7labs.trackomatic.domain.model.Session
import javax.inject.Inject

class InsertSession @Inject constructor(
    private val sessionRepository: MySessionRepository
) {
    suspend operator fun invoke(session: Session) = sessionRepository.insertSession(session)
}