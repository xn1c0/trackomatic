package com.n7labs.trackomatic.domain.usecase.validatable

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.inject.Inject

@Module
@InstallIn(ViewModelComponent::class)
class ValidateAge @Inject constructor() {
    @Provides
    @ViewModelScoped
    operator fun invoke(body: String): ValidationResult {

        // allowed characters
        val allowedCharacters: Pattern = Pattern.compile("^[1-9][0-9]+")
        val hasAllowedCharacters: Matcher = allowedCharacters.matcher(body)

        return when {
            body.isEmpty() -> ValidationResult.Invalid.EmptyError
            body.length > 3 -> ValidationResult.Invalid.NumberError.LengthLHS
            !hasAllowedCharacters.find() -> ValidationResult.Invalid.NumberError.DigitLHS
            else -> ValidationResult.Valid
        }
    }
}