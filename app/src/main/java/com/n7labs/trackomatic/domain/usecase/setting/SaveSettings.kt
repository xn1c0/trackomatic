package com.n7labs.trackomatic.domain.usecase.setting

import com.n7labs.trackomatic.data.repository.MySettingsRepository
import com.n7labs.trackomatic.domain.model.Settings
import javax.inject.Inject

class SaveSettings @Inject constructor(
    private val settingsRepository: MySettingsRepository
) {
    suspend operator fun invoke(settings: Settings) = settingsRepository.saveSettings(settings)
}