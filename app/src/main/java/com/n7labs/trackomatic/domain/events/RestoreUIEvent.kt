package com.n7labs.trackomatic.domain.events

sealed class RestoreUIEvent {
    data class EmailChanged(val email: String): RestoreUIEvent()
    object EmailFocusChanged: RestoreUIEvent()
    object Submit: RestoreUIEvent()
}