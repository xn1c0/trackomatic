package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.data.repository.MyAuthRepository
import javax.inject.Inject

class SignOut @Inject constructor(
    private val authRepository: MyAuthRepository
) {
    suspend operator fun invoke() = authRepository.fireSignOut()
}