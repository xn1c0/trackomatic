package com.n7labs.trackomatic.domain.usecase.validatable

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.inject.Inject

@Module
@InstallIn(ViewModelComponent::class)
class ValidateUsername @Inject constructor() {
    @Provides
    @ViewModelScoped
    operator fun invoke(username: String): ValidationResult {

        // allowed characters
        val allowedCharacters: Pattern = Pattern.compile("^[a-zA-Z0-9_]+")
        val hasAllowedCharacters: Matcher = allowedCharacters.matcher(username)

        return when {
            username.isEmpty() -> ValidationResult.Invalid.EmptyError
            (username.length < 3 || username.length > 20) -> ValidationResult.Invalid.UsernameError.Length
            username.contains(" ") -> ValidationResult.Invalid.UsernameError.WhiteSpace
            !hasAllowedCharacters.find() -> ValidationResult.Invalid.UsernameError.Special
            username.contains("__") -> ValidationResult.Invalid.UsernameError.RepeatedSpecial
            (username.startsWith("_") || username.endsWith("_")) ->
                ValidationResult.Invalid.UsernameError.PeaksSpecial
            else -> ValidationResult.Valid
        }
    }
}