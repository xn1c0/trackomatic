package com.n7labs.trackomatic.domain.usecase.images

import com.n7labs.trackomatic.data.repository.MyImageRepository
import javax.inject.Inject

class InsertImage @Inject constructor(
    private val imageRepository: MyImageRepository
) {
    suspend operator fun invoke(data: ByteArray) = imageRepository.addImageToFirebaseStorage(data)
}