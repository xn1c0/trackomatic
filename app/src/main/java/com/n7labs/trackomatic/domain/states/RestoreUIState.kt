package com.n7labs.trackomatic.domain.states

data class RestoreUIState(
    val email: String = "",
    val emailError: String? = null,
)