package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.domain.repository.AuthRepository
import com.n7labs.trackomatic.domain.repository.SendPasswordResetResponse
import javax.inject.Inject

class SendPasswordResetEmail @Inject constructor(
    private val authRepository: AuthRepository
) {
    suspend operator fun invoke(
        email: String
    ): SendPasswordResetResponse = authRepository.fireSendPasswordResetEmail(email)
}