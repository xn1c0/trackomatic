package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.domain.repository.AuthRepository
import javax.inject.Inject

class IsUserAuthenticated @Inject constructor(
    private val authRepository: AuthRepository
) {
    operator fun invoke(): Boolean = authRepository.isUserAuthenticatedInFireBase
}