package com.n7labs.trackomatic.domain.repository

import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.UserProfile
import kotlinx.coroutines.flow.Flow

/**
 * Bunch of aliases to better organize code, every single one of the below listed,
 * is used as repository response of any type of call to external api
 */

typealias ProfileSaveResponse = Response<Boolean>
typealias ProfileResponse = Response<UserProfile>

/**
 * Must have functionality to provide a full profile "modifications"
 */
interface ProfileRepository {

    val displayName: String
    val photoUrl: String

    suspend fun saveProfile(
        profile: UserProfile
    ): ProfileSaveResponse

    fun getProfiles(): Flow<ProfileResponse>

}