package com.n7labs.trackomatic.domain.usecase.validatable

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.inject.Inject

@Module
@InstallIn(ViewModelComponent::class)
class ValidateWH @Inject constructor() {
    @Provides
    @ViewModelScoped
    operator fun invoke(body: String): ValidationResult {

        val data = body.split(".")
        val decimal = data[0]

        // allowed characters
        val allowedCharactersLHS: Pattern = Pattern.compile("^[1-9][0-9]+")
        val hasAllowedCharactersLHS: Matcher = allowedCharactersLHS.matcher(decimal)

        var fractional = "0"
        if (data.size == 2)
            fractional = data[1]

        val allowedCharactersRHS: Pattern = Pattern.compile("^[0-9]+")
        val hasAllowedCharactersRHS: Matcher = allowedCharactersRHS.matcher(fractional)

        return when {
            body.isEmpty() -> ValidationResult.Invalid.EmptyError
            data.size > 2 -> ValidationResult.Invalid.NumberError.BadFormat
            !hasAllowedCharactersLHS.find() -> ValidationResult.Invalid.NumberError.DigitLHS
            !hasAllowedCharactersRHS.find() -> ValidationResult.Invalid.NumberError.DigitRHS
            decimal.length > 3 -> ValidationResult.Invalid.NumberError.LengthLHS
            fractional.length > 2 -> ValidationResult.Invalid.NumberError.LengthRHS
            else -> ValidationResult.Valid
        }
    }
}