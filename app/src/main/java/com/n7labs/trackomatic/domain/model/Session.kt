package com.n7labs.trackomatic.domain.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue
import java.util.*
@Parcelize
data class Session(
    val avgSpeedInMeters: Double = 0.0,
    val distanceInMeters: Double = 0.0,
    val timeRunInMillis: Long = 0L,
    val caloriesBurned: Double = 0.0,
    val listPolyLine: @RawValue List<GeoPoint>? = null,
    val timestamp: Timestamp = Timestamp(Date()),
    val mapConfig: MapConfig = MapConfig(),
    val uri: String? = null,
    val sessionID: String? = null,
    val userID: String? = null
): Parcelable