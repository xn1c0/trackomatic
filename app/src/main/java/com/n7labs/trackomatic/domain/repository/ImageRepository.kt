package com.n7labs.trackomatic.domain.repository

import android.net.Uri
import com.n7labs.trackomatic.domain.model.Response

typealias ImageAddResponse = Response<Uri>
typealias ImageRemoveResponse = Response<Boolean>

interface ImageRepository {
    suspend fun addImageToFirebaseStorage(data: ByteArray): ImageAddResponse
    suspend fun removeImageFromFirebaseStorage(uri: String): ImageRemoveResponse
}