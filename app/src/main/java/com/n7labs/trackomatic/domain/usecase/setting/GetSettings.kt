package com.n7labs.trackomatic.domain.usecase.setting

import com.n7labs.trackomatic.data.repository.MySettingsRepository
import javax.inject.Inject

class GetSettings @Inject constructor(
    private val settingsRepository: MySettingsRepository
) {
    operator fun invoke() = settingsRepository.getSettings()
}