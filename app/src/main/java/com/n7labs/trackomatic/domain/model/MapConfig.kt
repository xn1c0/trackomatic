package com.n7labs.trackomatic.domain.model

import android.os.Parcelable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import com.google.maps.android.compose.MapType
import com.n7labs.trackomatic.domain.model.types.PolyLineWidthType
import kotlinx.parcelize.Parcelize

@Parcelize
data class MapConfig(
    val style: MapType = MapType.NORMAL,
    val weight: PolyLineWidthType = PolyLineWidthType.MEDIUM,
    val colorValue: Int = Color.Black.toArgb(),
): Parcelable