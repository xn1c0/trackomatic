package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.domain.repository.AuthRepository
import com.n7labs.trackomatic.domain.repository.OneTapSignInResponse
import javax.inject.Inject

class OneTapSignInWithGoogle @Inject constructor(
    private val authRepository: AuthRepository
) {
    suspend operator fun invoke(): OneTapSignInResponse = authRepository.fireOneTapSignInWithGoogle()
}