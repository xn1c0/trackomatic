package com.n7labs.trackomatic.domain.usecase.session

import com.n7labs.trackomatic.data.repository.MySessionRepository
import javax.inject.Inject


class DeleteSession @Inject constructor(
    private val sessionRepository: MySessionRepository
) {
    suspend operator fun invoke(sessionID: String) = sessionRepository.deleteSession(sessionID)
}
