package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.domain.repository.AuthRepository
import com.n7labs.trackomatic.domain.repository.CreateUserWithEmailAndPasswordResponse
import javax.inject.Inject

class CreateUserWithEmailAndPassword @Inject constructor(
    private val authRepository: AuthRepository
) {
    suspend operator fun invoke(
        username: String,
        email: String,
        password: String
    ): CreateUserWithEmailAndPasswordResponse = authRepository
        .fireCreateUserWithEmailAndPassword(username, email, password)
}