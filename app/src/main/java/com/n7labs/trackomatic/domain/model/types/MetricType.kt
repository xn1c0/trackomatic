package com.n7labs.trackomatic.domain.model.types

enum class MetricType(val value: String) {
    METERS("m/ms/cal"),
    KILOS("km/kmh/kcal")
}