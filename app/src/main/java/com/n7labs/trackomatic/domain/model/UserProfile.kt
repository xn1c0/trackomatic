package com.n7labs.trackomatic.domain.model

import com.n7labs.trackomatic.domain.model.types.GenderType

data class UserProfile(
    val age: Int = 25,
    val gender: GenderType = GenderType.MALE,
    val height: Float = 180f,
    val weight: Float = 70f,
)
