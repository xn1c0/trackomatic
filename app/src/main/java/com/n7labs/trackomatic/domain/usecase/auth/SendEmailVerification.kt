package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.data.repository.MyAuthRepository
import com.n7labs.trackomatic.domain.repository.SendEmailVerificationResponse
import javax.inject.Inject

class SendEmailVerification @Inject constructor(
    private val authRepository: MyAuthRepository
) {
    suspend operator fun invoke(): SendEmailVerificationResponse =
        authRepository.fireSendEmailVerification()
}