package com.n7labs.trackomatic.domain.usecase.validatable

sealed class ValidationResult(
    val isSuccessful: Boolean,
    val errorMessage: String? = null
) {

    object Valid: ValidationResult(
        isSuccessful = true
    )

    sealed class Invalid {

        object EmptyError: ValidationResult(
            isSuccessful = false,
            errorMessage = "Empty field"
        )

        object TermsError: ValidationResult(
            isSuccessful = false,
            errorMessage = "Please accept terms"
        )

        sealed class EmailError {
            object Wrong: ValidationResult(
                isSuccessful = false,
                errorMessage = "This is not a valid email"
            )
        }

        sealed class PasswordError {
            object Length: ValidationResult(
                isSuccessful = false,
                errorMessage = "At least 8 character required"
            )
            object LowerCase: ValidationResult(
                isSuccessful = false,
                errorMessage = "A lower case letter must occur at least once"
            )
            object UpperCase: ValidationResult(
                isSuccessful = false,
                errorMessage = "An upper case letter must occur at least once"
            )
            object Digit: ValidationResult(
                isSuccessful = false,
                errorMessage = "A digit must occur at least once"
            )
            object Special: ValidationResult(
                isSuccessful = false,
                errorMessage = "A special character must occur at least once"
            )
            object WhiteSpace: ValidationResult(
                isSuccessful = false,
                errorMessage = "Whitespaces are not allowed"
            )
            object Check: ValidationResult(
                isSuccessful = false,
                errorMessage = "Passwords do not match"
            )
        }

        sealed class UsernameError {
            object Length: ValidationResult(
                isSuccessful = false,
                errorMessage = "At least 3 and up to 20 characters"
            )
            object Special: ValidationResult(
                isSuccessful = false,
                errorMessage = "Only special characters allowed are . and _"
            )
            object RepeatedSpecial: ValidationResult(
                isSuccessful = false,
                errorMessage = "Repeated specials are not allowed"
            )
            object WhiteSpace: ValidationResult(
                isSuccessful = false,
                errorMessage = "Whitespaces are not allowed"
            )
            object PeaksSpecial: ValidationResult(
                isSuccessful = false,
                errorMessage = ". and _ are not allowed at the beginning or ending"
            )
        }

        sealed class NumberError {
            object LengthLHS: ValidationResult(
                isSuccessful = false,
                errorMessage = "Must be less than 4 decimal digits"
            )
            object LengthRHS: ValidationResult(
                isSuccessful = false,
                errorMessage = "Only 2 fractional digits"
            )
            object DigitLHS: ValidationResult(
                isSuccessful = false,
                errorMessage = "Bad decimal format"
            )
            object DigitRHS: ValidationResult(
                isSuccessful = false,
                errorMessage = "Bad fractional format"
            )
            object BadFormat: ValidationResult(
                isSuccessful = false,
                errorMessage = "Bad format"
            )

        }
    }
}