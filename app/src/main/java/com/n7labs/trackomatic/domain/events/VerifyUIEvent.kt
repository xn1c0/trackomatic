package com.n7labs.trackomatic.domain.events

sealed class VerifyUIEvent {
    object SignOut: VerifyUIEvent()
    object SendVerification: VerifyUIEvent()
}