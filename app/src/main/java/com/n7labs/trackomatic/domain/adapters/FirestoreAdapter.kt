package com.n7labs.trackomatic.domain.adapters

import com.n7labs.trackomatic.core.Constants
import com.n7labs.trackomatic.domain.model.Session
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.domain.model.UserProfile

/**
 * Helps repositories to map data to be consumed by firebase
 */
class FirestoreAdapter {
    operator fun invoke(session: Session, sessionID: String, userID: String) = hashMapOf(
        Constants.CALORIES_FIELD to session.caloriesBurned,
        Constants.TIMESTAMP_FIELD to session.timestamp,
        Constants.DISTANCE_FIELD to session.distanceInMeters,
        Constants.TIME_FIELD to session.timeRunInMillis,
        Constants.SPEED_FIELD to session.avgSpeedInMeters,
        Constants.POLYLINES_FIELD to session.listPolyLine,
        Constants.MAP_CONFIG_FIELD to session.mapConfig,
        Constants.URI_FIELD to session.uri,
        Constants.SESSION_ID_FIELD to sessionID,
        Constants.USERID_FIELD to userID,
    )

    operator fun invoke(setting: Settings, userID: String) = hashMapOf(
        Constants.MAP_CONFIG_FIELD to setting.mapConfig,
        Constants.METRICS_FIELD to setting.metrics,
        Constants.USERID_FIELD to userID,
    )

    operator fun invoke(profile: UserProfile, userID: String) = hashMapOf(
        Constants.AGE_FIELD to profile.age,
        Constants.GENDER_FIELD to profile.gender,
        Constants.HEIGHT_FIELD to profile.height,
        Constants.WEIGHT_FIELD to profile.weight,
        Constants.USERID_FIELD to userID,
    )
}