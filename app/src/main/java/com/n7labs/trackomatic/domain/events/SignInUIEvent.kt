package com.n7labs.trackomatic.domain.events

import com.google.firebase.auth.AuthCredential

sealed class SignInUIEvent {
    data class EmailChanged(val email: String): SignInUIEvent()
    data class PasswordChanged(val password: String): SignInUIEvent()
    object EmailFocusChanged: SignInUIEvent()
    object PasswordFocusChanged: SignInUIEvent()
    object BeginGoogleSignIn: SignInUIEvent()
    data class CompleteGoogleSignIn(val googleCredential: AuthCredential): SignInUIEvent()
    object EmailSignIn: SignInUIEvent()
}