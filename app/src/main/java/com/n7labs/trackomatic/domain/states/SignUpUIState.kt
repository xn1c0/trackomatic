package com.n7labs.trackomatic.domain.states

data class SignUpUIState(
    val username: String = "",
    val usernameError: String? = null,
    val email: String = "",
    val emailError: String? = null,
    val password: String = "",
    val passwordError: String? = null,
    val passwordCheck: String = "",
    val passwordCheckError: String? = null,
    val acceptedTerms: Boolean = false,
    val acceptedTermsError: String? = null
)