package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.domain.repository.AuthRepository
import com.n7labs.trackomatic.domain.repository.SignInWithEmailAndPasswordResponse
import javax.inject.Inject

class SignInWithEmailAndPassword @Inject constructor(
    private val authRepository: AuthRepository
) {
    suspend operator fun invoke(
        email: String,
        password: String
    ): SignInWithEmailAndPasswordResponse =
        authRepository.fireSignInWithEmailAndPassword(email, password)
}