package com.n7labs.trackomatic.domain.usecase.validatable

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.inject.Inject


@Module
@InstallIn(ViewModelComponent::class)
class ValidatePassword @Inject constructor() {
    @Provides
    @ViewModelScoped
    operator fun invoke(password: String): ValidationResult {

        val lowerCaseLetter: Pattern = Pattern.compile("[a-z]")
        val upperCaseLetter: Pattern = Pattern.compile("[A-Z]")
        val digit: Pattern = Pattern.compile("[0-9]")
        val special: Pattern = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]")

        val hasLowerCaseLetter: Matcher = lowerCaseLetter.matcher(password)
        val hasUpperCaseLetter: Matcher = upperCaseLetter.matcher(password)
        val hasDigit: Matcher = digit.matcher(password)
        val hasSpecial: Matcher = special.matcher(password)

        return when {
            password.isEmpty() -> ValidationResult.Invalid.EmptyError
            password.length < 8 -> ValidationResult.Invalid.PasswordError.Length
            password.contains(" ") -> ValidationResult.Invalid.PasswordError.WhiteSpace
            !hasLowerCaseLetter.find() -> ValidationResult.Invalid.PasswordError.LowerCase
            !hasUpperCaseLetter.find() -> ValidationResult.Invalid.PasswordError.UpperCase
            !hasDigit.find() -> ValidationResult.Invalid.PasswordError.Digit
            !hasSpecial.find() -> ValidationResult.Invalid.PasswordError.Special
            else -> ValidationResult.Valid
        }
    }
}