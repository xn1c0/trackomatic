package com.n7labs.trackomatic.domain.usecase.validatable

import android.util.Patterns
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@Module
@InstallIn(ViewModelComponent::class)
class ValidateEmail @Inject constructor() {
    @Provides
    @ViewModelScoped
    operator fun invoke(email: String): ValidationResult {
        return when {
            email.isEmpty() -> ValidationResult.Invalid.EmptyError
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() ->
                ValidationResult.Invalid.EmailError.Wrong
            else -> ValidationResult.Valid
        }
    }
}