package com.n7labs.trackomatic.domain.usecase.profile

import com.n7labs.trackomatic.data.repository.MyProfileRepository
import com.n7labs.trackomatic.domain.model.UserProfile
import javax.inject.Inject
class SaveProfile @Inject constructor(
    private val profileRepository: MyProfileRepository
) {
    suspend operator fun invoke(userProfile: UserProfile) = profileRepository.saveProfile(userProfile)
}