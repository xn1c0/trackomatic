package com.n7labs.trackomatic.domain.usecase.validatable

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@Module
@InstallIn(ViewModelComponent::class)
class ValidatePasswordCheck @Inject constructor() {
    @Provides
    @ViewModelScoped
    operator fun invoke(
        password: String,
        passwordCheck: String
    ): ValidationResult {
        return when {
            passwordCheck.isEmpty() -> ValidationResult.Invalid.EmptyError
            password != passwordCheck -> ValidationResult.Invalid.PasswordError.Check
            else -> ValidationResult.Valid
        }
    }
}