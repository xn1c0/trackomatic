package com.n7labs.trackomatic.domain.model

import android.os.Parcelable
import com.n7labs.trackomatic.domain.model.types.MetricType
import kotlinx.parcelize.Parcelize

@Parcelize
data class Settings (
    var mapConfig: MapConfig = MapConfig(),
    var metrics: MetricType = MetricType.METERS,
    var userID: String? = null,
): Parcelable