package com.n7labs.trackomatic.domain.usecase.auth

import com.google.firebase.auth.AuthCredential
import com.n7labs.trackomatic.domain.repository.AuthRepository
import com.n7labs.trackomatic.domain.repository.SignInWithGoogleResponse
import javax.inject.Inject

class SignInWithGoogle @Inject constructor(
    private val authRepository: AuthRepository
) {
    suspend operator fun invoke(
        googleCredential: AuthCredential
    ): SignInWithGoogleResponse = authRepository.fireSignInWithGoogle(googleCredential)
}