package com.n7labs.trackomatic.domain.events

import androidx.compose.ui.graphics.Color
import com.google.maps.android.compose.MapType
import com.n7labs.trackomatic.domain.model.Settings
import com.n7labs.trackomatic.domain.model.UserProfile
import com.n7labs.trackomatic.domain.model.types.GenderType
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.domain.model.types.PolyLineWidthType

sealed class SettingsUIEvent {
    data class AgeChanged(val age: String): SettingsUIEvent()
    data class HeightChanged(val height: String): SettingsUIEvent()
    data class WeightChanged(val weight: String): SettingsUIEvent()
    data class GenderChanged(val gender: GenderType): SettingsUIEvent()
    data class StyleChanged(val style: MapType): SettingsUIEvent()
    data class LineWeightChanged(val lineWeight: PolyLineWidthType): SettingsUIEvent()
    data class LineColorChanged(val lineColor: Color): SettingsUIEvent()
    data class MetricsChanged(val metrics: MetricType): SettingsUIEvent()
    data class UserIDChanged(val userID: String): SettingsUIEvent()

    data class SettingsChanged(val settings: Settings): SettingsUIEvent()
    data class ProfileChanged(val userProfile: UserProfile): SettingsUIEvent()

    object ResetProfileState: SettingsUIEvent()
    object ResetProfileSaveResponse: SettingsUIEvent()

    object AgeFocusChanged: SettingsUIEvent()
    object HeightFocusChanged: SettingsUIEvent()
    object WeightFocusChanged: SettingsUIEvent()
    object SignOut: SettingsUIEvent()
    object RevokeGoogleAccess: SettingsUIEvent()
    object DeleteAccount: SettingsUIEvent()
    object SubmitProfile: SettingsUIEvent()
}