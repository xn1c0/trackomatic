package com.n7labs.trackomatic.domain.usecase.session

import com.n7labs.trackomatic.data.repository.MySessionRepository
import javax.inject.Inject

class GetSessionsByDate @Inject constructor(
    private val sessionRepository: MySessionRepository
) {
    operator fun invoke() = sessionRepository.getListSessionsByDate()
}