package com.n7labs.trackomatic.domain.repository

import com.n7labs.trackomatic.domain.model.Response
import com.n7labs.trackomatic.domain.model.Session
import kotlinx.coroutines.flow.Flow

typealias InsertSessionResponse = Response<Boolean>
typealias DeleteSessionResponse = Response<Boolean>

typealias Sessions = List<Session>
typealias SessionResponse = Response<Sessions>

interface SessionRepository {
    suspend fun insertSession(session: Session): InsertSessionResponse
    suspend fun deleteSession(sessionID: String): DeleteSessionResponse
    fun getListSessionsByDate(): Flow<SessionResponse>
}