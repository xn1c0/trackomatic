package com.n7labs.trackomatic.domain.usecase.auth

import com.n7labs.trackomatic.data.repository.MyAuthRepository
import javax.inject.Inject

class IsUserEmailVerified @Inject constructor(
    private val authRepository: MyAuthRepository
) {
    operator fun invoke(): Boolean = authRepository.isUserEmailVerifiedInFireBase
}