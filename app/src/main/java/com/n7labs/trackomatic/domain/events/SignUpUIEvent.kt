package com.n7labs.trackomatic.domain.events

sealed class SignUpUIEvent {
    data class UsernameChanged(val username: String): SignUpUIEvent()
    data class EmailChanged(val email: String): SignUpUIEvent()
    data class PasswordChanged(val password: String): SignUpUIEvent()
    data class PasswordCheckChanged(val passwordCheck: String): SignUpUIEvent()
    data class TermsChanged(val acceptedTerms: Boolean): SignUpUIEvent()
    object UsernameFocusChanged: SignUpUIEvent()
    object EmailFocusChanged: SignUpUIEvent()
    object PasswordFocusChanged: SignUpUIEvent()
    object PasswordCheckFocusChanged: SignUpUIEvent()
    object SignUp: SignUpUIEvent()
}