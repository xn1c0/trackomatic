package com.n7labs.trackomatic.domain.model.types

enum class PolyLineWidthType(val value: Float) {
    SMALL(4f),
    MEDIUM(7f),
    BIG(10f)
}