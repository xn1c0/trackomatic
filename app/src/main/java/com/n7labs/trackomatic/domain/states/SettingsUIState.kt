package com.n7labs.trackomatic.domain.states

import androidx.compose.ui.graphics.Color
import com.google.maps.android.compose.MapType
import com.n7labs.trackomatic.domain.model.types.GenderType
import com.n7labs.trackomatic.domain.model.types.MetricType
import com.n7labs.trackomatic.domain.model.types.PolyLineWidthType

data class SettingsUIState(
    /**
     * User Info's
     */
    val age: String = "",
    val height: String = "",
    val weight: String = "",
    val gender: GenderType = GenderType.MALE,
    val ageError: String? = null,
    val heightError: String? = null,
    val weightError: String? = null,

    /**
     * Settings
     */
    val style: MapType = MapType.NORMAL,
    val lineWeight: PolyLineWidthType = PolyLineWidthType.MEDIUM,
    val lineColor: Color = Color.Black,
    val metrics: MetricType = MetricType.METERS,
    val userID: String? = null,
)