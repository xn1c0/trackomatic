package com.n7labs.trackomatic.domain.repository

import com.google.android.gms.auth.api.identity.BeginSignInResult
import com.google.firebase.auth.AuthCredential
import com.n7labs.trackomatic.domain.model.Response

/**
 * Bunch of aliases to better organize code, every single one of the below listed,
 * is used as repository response of any type of call to external api
 */
typealias OneTapSignInResponse = Response<BeginSignInResult>
typealias SignInWithGoogleResponse = Response<Boolean>
typealias SignInWithEmailAndPasswordResponse = Response<Boolean>
typealias CreateUserWithEmailAndPasswordResponse = Response<Boolean>
typealias SendPasswordResetResponse = Response<Boolean>

typealias SignOutResponse = Response<Boolean>
typealias RevokeAccessResponse = Response<Boolean>
typealias DeleteAccountResponse = Response<Boolean>
typealias SendEmailVerificationResponse = Response<Boolean>

/**
 * Must have functionality to provide a full authentication mechanism
 */
interface AuthRepository {

    val isUserAuthenticatedInFireBase: Boolean
    val isUserEmailVerifiedInFireBase: Boolean

    suspend fun fireOneTapSignInWithGoogle(): OneTapSignInResponse

    suspend fun fireSignInWithGoogle(
        googleCredential: AuthCredential
    ): SignInWithGoogleResponse

    suspend fun fireSignInWithEmailAndPassword(
        email: String,
        password: String
    ): SignInWithEmailAndPasswordResponse

    suspend fun fireCreateUserWithEmailAndPassword(
        username:String,
        email: String,
        password: String
    ): CreateUserWithEmailAndPasswordResponse

    suspend fun fireSendPasswordResetEmail(
        email: String,
    ): SendPasswordResetResponse

    suspend fun fireSignOut(): SignOutResponse

    suspend fun fireRevokeAccess(): RevokeAccessResponse

    suspend fun fireDeleteAccount(): DeleteAccountResponse

    suspend fun fireSendEmailVerification(): SendEmailVerificationResponse

}