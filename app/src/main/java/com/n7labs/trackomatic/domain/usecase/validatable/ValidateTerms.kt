package com.n7labs.trackomatic.domain.usecase.validatable

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@Module
@InstallIn(ViewModelComponent::class)
class ValidateTerms @Inject constructor() {
    @Provides
    @ViewModelScoped
    operator fun invoke(isAccepted: Boolean): ValidationResult {
        return if (isAccepted) ValidationResult.Valid else ValidationResult.Invalid.TermsError
    }
}