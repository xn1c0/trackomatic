package com.n7labs.trackomatic.domain.usecase.profile

import com.n7labs.trackomatic.data.repository.MyProfileRepository
import javax.inject.Inject

class GetProfile @Inject constructor(
    private val profileRepository: MyProfileRepository
) {
    operator fun invoke() = profileRepository.getProfiles()
}