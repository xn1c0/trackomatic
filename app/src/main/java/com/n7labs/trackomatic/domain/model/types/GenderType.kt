package com.n7labs.trackomatic.domain.model.types

enum class GenderType {
    MALE, FEMALE
}